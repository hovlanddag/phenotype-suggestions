package no.uio.ifi.logid.rekvisisjon;

import static org.junit.Assert.*;

import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.util.List;
import java.util.Map;

import org.junit.Test;
import org.springframework.mock.web.MockServletContext;

import no.uio.ifi.logid.rekvisisjon.*;
import no.uio.ifi.logid.rekvisisjon.hpo.HPOException;
import no.uio.ifi.logid.rekvisisjon.hpo.OntologyFactory;
import no.uio.ifi.logid.rekvisisjon.hpo.PhenotypeList;
import no.uio.ifi.logid.rekvisisjon.hpo.Tickbox;

public class PhenotypeListReaderTest {

	@Test
	public void testPost() throws IOException, HPOException, InterruptedException {
		String path = "src/test/resources";
		
		OntologyFactory.init(path, OntologyFactory.backend_name.Sequoia);
		PhenotypeListReader l = PhenotypeListReader.getInstance(path);
		PhenotypeList list = l.getTickboxes();
		Map<String, List<Tickbox>> tiboxes= list.getList();
		System.out.println(tiboxes.keySet());
		assertTrue(tiboxes.keySet().size() > 10);
		assertTrue(tiboxes.get("VEKST OG UTVIKLING") != null);
		assertTrue(tiboxes.get("VEKST OG UTVIKLING").get(0) != null);
	}

	
}
