package no.uio.ifi.logid.rekvisisjon;

import static org.junit.Assert.*;

import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.junit.Test;
import no.uio.ifi.logid.rekvisisjon.*;
import no.uio.ifi.logid.rekvisisjon.hpo.HPAnnotation;
import no.uio.ifi.logid.rekvisisjon.hpo.HPAnnotationsFileReader;
import no.uio.ifi.logid.rekvisisjon.hpo.HPOException;
import no.uio.ifi.logid.rekvisisjon.hpo.PhenotypeList;
import no.uio.ifi.logid.rekvisisjon.hpo.Tickbox;

public class HPAnnotationsFIleReaderTest {

	@Test
	public void testPost() throws IOException, HPOException {
		String path = "src/test/resources";
		
		HPAnnotationsFileReader l = HPAnnotationsFileReader.getInstance(path);
		Set<HPAnnotation> a1 = l.getAnnotations("HP:0001956");
		assertTrue(a1.size() >= 1);
		assertTrue(a1.iterator().next().getDb_name() != null);
		for(HPAnnotation ann : a1)
			assertTrue(ann.getHpo_id().equals("HP_0001956"));
	}

	@Test
	public void testDiseaseCodes() throws IOException, HPOException {
		String path = "src/test/resources";
		HPAnnotationsFileReader l = HPAnnotationsFileReader.getInstance(path);
		Set<String> a1 = l.getDiseaseAnnotations("OMIM:300100");
		assertTrue(a1.contains("HP_0001250"));
		assertTrue(a1.contains("HP_0003676"));
	}

	
}
