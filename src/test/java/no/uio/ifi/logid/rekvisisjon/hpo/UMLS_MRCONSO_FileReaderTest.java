package no.uio.ifi.logid.rekvisisjon.hpo;

import static org.junit.Assert.*;

import java.io.IOException;
import java.util.Set;

import org.junit.Test;

public class UMLS_MRCONSO_FileReaderTest {

	
	
	@Test
	public void test() throws IOException {
		UMLS_MRCONSO_FileReader umls = UMLS_MRCONSO_FileReader.getInstance("src/test/resources");
		String omim = umls.getOMIMCode("C1527231");
		assertTrue(omim.equals("OMIM:300100"));
		HPAnnotationsFileReader annots = HPAnnotationsFileReader.getInstance("src/test/resources");
		Set<String> symptoms =  annots.getDiseaseAnnotations(omim);
		assertTrue(symptoms.contains("HP_0003676"));
	}

}
