package no.uio.ifi.logid.rekvisisjon;

import static org.junit.Assert.*;

import java.io.IOException;
import java.util.List;
import java.util.Set;

import org.junit.Test;
import org.semanticweb.owlapi.model.IRI;
import org.semanticweb.owlapi.model.OWLAnnotationProperty;
import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLOntologyStorageException;

import no.uio.ifi.logid.rekvisisjon.hpo.Diagnosis;
import no.uio.ifi.logid.rekvisisjon.hpo.DiagnosisBean;
import no.uio.ifi.logid.rekvisisjon.hpo.HPAnnotation;
import no.uio.ifi.logid.rekvisisjon.hpo.HPAnnotationsFileReader;
import no.uio.ifi.logid.rekvisisjon.hpo.HPOException;
import no.uio.ifi.logid.rekvisisjon.hpo.HPUtils;
import no.uio.ifi.logid.rekvisisjon.hpo.OntologyFactory;
import no.uio.ifi.logid.rekvisisjon.hpo.OntologyFactory.backend_name;
import no.uio.ifi.logid.rekvisisjon.hpo.PhenotypeOntology;
import no.uio.ifi.logid.rekvisisjon.hpo.PhenotypeOntologySequoia;
import no.uio.ifi.logid.rekvisisjon.hpo.PhenotypeState;
import no.uio.ifi.logid.rekvisisjon.hpo.Tickbox;

public class PhenotypeSequoiaTest {

	public static String config_path = "src/test/resources";
	
	// Synchronization isse? IllegalStateException
	//@Test
	public void testFirstTimeSetup() throws IOException{
		PhenotypeOntologySequoia.firstTimeSetup(config_path);
		
	}
	
	@Test
	public void testListReader() throws IOException, HPOException, InterruptedException {
		OntologyFactory.init(config_path, backend_name.Sequoia);
		PhenotypeListReader.getInstance(config_path);
		PhenotypeOntology hpowl = OntologyFactory.getOntology();
		List<Tickbox> tops = hpowl.getTopClassTickboxes(true);
		assertFalse(tops.isEmpty());
		assertTrue(tops.size() == 1);
		List<Tickbox> patients = hpowl.getPatientIDList();
		assertTrue(patients != null);
	}
	
	@Test
	public void annotationReaderInit() throws IOException, OWLOntologyStorageException, HPOException {
		HPAnnotationsFileReader annotr = HPAnnotationsFileReader.getInstance(config_path);
		assertTrue(annotr != null);
		
	}
	
	@Test
	public void annotationReaderGet() throws IOException, OWLOntologyStorageException, HPOException {
		HPAnnotationsFileReader annotr = HPAnnotationsFileReader.getInstance(config_path);
		assertTrue(annotr != null);
		Set<HPAnnotation> annots = annotr.getAnnotations("HP_0001939");
		assertTrue(annots != null);
		boolean foundOsteo = false;
		for(HPAnnotation annot : annots){
			if(annot.getDb_name().equals("Osteomesopyknosis")){
				foundOsteo = true;
				break;
			}
		}
		assertTrue(foundOsteo);
	}
	
	@Test
	public void testUMLS() throws IOException, HPOException, InterruptedException {
		OntologyFactory.init(config_path, backend_name.Sequoia);
		PhenotypeOntologySequoia hpowl = (PhenotypeOntologySequoia) OntologyFactory.getOntology();
		OWLClass cls = hpowl.getUMLSEntry("UMLS:C4025901");
		assertTrue(cls != null);
		OWLClass hp2 = hpowl.getOntClass("HP_0000002");
		assertTrue(hp2.equals(cls));
	}
	
	@Test
	public void patientTest() throws IOException, OWLOntologyStorageException, HPOException, InterruptedException {
		OntologyFactory.init(config_path, backend_name.Sequoia);
		PhenotypeOntologySequoia hpowl = (PhenotypeOntologySequoia) OntologyFactory.getOntology();
		HPAnnotationsFileReader.getInstance(config_path);
		
		assertFalse(hpowl.hasSubclass("HP_0100758"));
		assertTrue(hpowl.hasSubclass("HP_0025144"));
		
		PhenotypeState ps = new PhenotypeState();
		assertFalse(ps.patientHasNegPhenotype("HP_0100536"));
		assertTrue(ps.setNegativePatientPhenotype("HP_0100536"));
		assertTrue(ps.patientHasNegPhenotype( "HP_0100536"));
		assertTrue(ps.setPositivePatientPhenotype( "HP_0001939"));
		assertTrue(ps.patientHasPhenotype("HP_0001939"));

		Set<DiagnosisBean> dis = ps.getAnnotations();
		assertTrue(dis.size() > 10);

		
		assertFalse(ps.setNegativePatientPhenotype( "HP_0001939"));
		ps.removePatientPositivePhenotype( "HP_0001939");
		assertFalse(ps.patientHasPhenotype( "HP_0001939"));
		assertTrue(ps.setNegativePatientPhenotype( "HP_0001939"));
		assertTrue(ps.patientHasNegPhenotype(  "HP_0001939"));
		ps.removePatientNegPhenotype("HP_0001939");
		assertFalse(ps.patientHasNegPhenotype("HP_0001939"));
		
		
	}
		

	
}
