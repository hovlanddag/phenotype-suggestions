/**
 * 
 */
package no.uio.ifi.logid.rekvisisjon;

import static org.junit.Assert.*;

import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;

import org.apache.jena.ontology.OntClass;
import org.apache.jena.rdf.model.Resource;
import org.junit.Test;
import org.semanticweb.owlapi.model.OWLClass;

import no.uio.ifi.logid.rekvisisjon.hpo.HPOException;
import no.uio.ifi.logid.rekvisisjon.hpo.PhenotypeOntologyFile;
import no.uio.ifi.logid.rekvisisjon.hpo.Tickbox;

/**
 * @author dag
 *
 */
public class PhenotypeOntologyFileTest {

	public static String config_path = "src/test/resources";
	/**
	 * Test method for {@link no.uio.ifi.logid.rekvisisjon.hpo.PhenotypeOntologyTDB2#getInstance(java.lang.String)}.
	 * @throws IOException 
	 * @throws HPOException 
	 */
	@Test
	public void testGetInstance() throws IOException, HPOException {
		PhenotypeOntologyFile ont = PhenotypeOntologyFile.getInstance(config_path);
		List<Resource> subs = ont.getSubclasses("HP_0000118", true);
		assertTrue(subs.size() > 1);

	}

	@Test
	public void testGetThings() throws IOException, HPOException {
		PhenotypeOntologyFile ont = PhenotypeOntologyFile.getInstance(config_path);
		List<Tickbox> subs = ont.getTopClassTickboxes(true);
		assertTrue(subs.size() > 1);

	}


	@Test
	public void testGetHPOEntries() throws IOException, HPOException {
		PhenotypeOntologyFile ont = PhenotypeOntologyFile.getInstance(config_path);
		List<Tickbox> subs = ont.getSubHPOEntries("", "HP_0008872", true);
		//assertTrue(cls.getLabel().equals("Feeding difficulties in infancy"));
		assertTrue(subs.size() > 0);
		for(Tickbox tb : subs) {
			String localname = tb.getHpoLocalName();
			OntClass cl = ont.getOntClass(localname);
			assertTrue(cl != null);
			
			assertFalse(tb.getHasSubclasses());
		}

	}
	@Test
	public void testGetHPOEntries2() throws IOException, HPOException {
		PhenotypeOntologyFile ont = PhenotypeOntologyFile.getInstance(config_path);
		List<Tickbox> subs = ont.getSubHPOEntries("", "HP:0030884", true);
		assertTrue(subs.size() == 0);
		for(Tickbox tb : subs) {
			String localname = tb.getHpoLocalName();
			OntClass cl = ont.getOntClass(localname);
			assertTrue(cl != null);
		}

	}
	
	public void testGetTops() throws IOException, HPOException{
		PhenotypeOntologyFile ont = PhenotypeOntologyFile.getInstance(config_path);
		List<Tickbox> subs = ont.getTopClassTickboxes(true);
		assertTrue(subs.size() > 0);
		
	}
	
}

