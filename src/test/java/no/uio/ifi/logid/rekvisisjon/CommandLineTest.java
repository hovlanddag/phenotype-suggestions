package no.uio.ifi.logid.rekvisisjon;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLOntologyStorageException;

import no.uio.ifi.logid.rekvisisjon.hpo.HPAnnotationsFileReader;
import no.uio.ifi.logid.rekvisisjon.hpo.HPOException;
import no.uio.ifi.logid.rekvisisjon.hpo.PhenotypeTree;
import no.uio.ifi.logid.rekvisisjon.hpo.Tickbox;
import no.uio.ifi.logid.rekvisisjon.hpo.UMLS_MRCONSO_FileReader;

public class CommandLineTest {
	private CommandLine cmd; 
	private static String test_resource_dir = "/var/lib/rekvisisjon";

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		CommandLine _cmd = new CommandLine(test_resource_dir);
		_cmd.init();
	}

	
	@Before
	public void setUp() throws Exception {
		this.cmd = new CommandLine(test_resource_dir);
		this.cmd.init();
	}

	@Test
	public void testSetUMLSPheno() throws IllegalArgumentException, OWLOntologyStorageException, IOException, HPOException {
		cmd.receiveCommand("setPhenotype UMLS:C0262655 YES");
		String out = cmd.receiveCommand("listPhenotypes");
		assertTrue(out.length() > 2);
		assertTrue(out.contains("HP_0000010"));
	}

	@Test
	public void testSetPhenotypeList() throws IllegalArgumentException, OWLOntologyStorageException, IOException, HPOException {
		cmd.receiveCommand("setPhenotypeList UMLS:C0262655");
		String out = cmd.receiveCommand("listPhenotypes");
		assertTrue(out.length() > 2);
		assertTrue(out.contains("HP_0000010"));
	}

	
	@Test
	public void testIllegalUMLSPheno() throws IllegalArgumentException, OWLOntologyStorageException, IOException, HPOException {
		boolean exception_thrown = false;
		try {
			cmd.receiveCommand("setPhenotype UMLS:XXXXXX YES");
		} catch(IllegalArgumentException e) {
			exception_thrown = true;
		}
		assertTrue(exception_thrown);
	}
	
	@Test
	public void testSetSinglePheno() throws IllegalArgumentException, OWLOntologyStorageException, IOException, HPOException {
		cmd.receiveCommand("setPhenotype HP:0001511 YES");
		String out = cmd.receiveCommand("listPhenotypes");
		assertTrue(out.length() > 2);
	}


	@Test
	public void testGetSuggestions() throws IllegalArgumentException, OWLOntologyStorageException, IOException, HPOException {
		cmd.receiveCommand("setPhenotype HP:0001283 YES");
		cmd.receiveCommand("setPhenotype HP:0001511 YES");
		String sugg = cmd.receiveCommand("getSuggestions");
		assertTrue(sugg.contains("HP_0002875"));
		assertFalse(sugg.contains("HP:0001511"));
	}

	@Test
	public void testBreastCarcinome() throws IllegalArgumentException, OWLOntologyStorageException, IOException, HPOException {
		cmd.receiveCommand("setPhenotype HP:0003002 YES");
		String sugg = cmd.receiveCommand("getSuggestions");
		assertTrue(sugg.contains("HP_0002875"));
	}


	@Test
	public void testReadMMIFile() throws IllegalArgumentException, OWLOntologyStorageException, IOException, HPOException {
		cmd.receiveCommand("readMMIFile src/test/resources/test.mmi");
		String sugg = cmd.receiveCommand("listPhenotypes");
		System.out.println(sugg);
		//assertTrue(sugg.contains("HP_0000577"));
	}

	@Test
	public void testPrecision() throws IllegalArgumentException, OWLOntologyStorageException, IOException, HPOException {
		cmd.receiveCommand("setPhenotype HP:0001283 YES");
		Set<OWLClass> symptom_set = new HashSet<OWLClass>();
		List<OWLClass> symptom_list = new ArrayList<OWLClass>();
		List<Tickbox> ticks = cmd.getPs().getSymptomSuggestions(); 
		for(Tickbox sugg : ticks) {
			OWLClass symp = cmd.getHPOEntry(sugg.getHpoLocalName());
			symptom_list.add(symp);
			symptom_set.add(symp);
		}
		float p = cmd.precision(symptom_set, symptom_list);
		assertTrue(p==1.0);
		p = cmd.precision(new HashSet<OWLClass>(), symptom_list);
		assertTrue(p==0.0);
		p = cmd.precision(symptom_set, new ArrayList<OWLClass>());
		assertTrue(p==0.0);
		p = cmd.precision(symptom_set, symptom_list.subList(0, 1));
		assertTrue(p==1.0);
		Set<OWLClass> singleton = new HashSet<OWLClass>();
		singleton.add(symptom_set.iterator().next());
		p = cmd.precision(singleton, symptom_list);
		assertTrue((p - 1.0/(float) symptom_list.size()) < 0.001);
	}


	@Test
	public void testAveragePrecision() throws IllegalArgumentException, OWLOntologyStorageException, IOException, HPOException {
		cmd.receiveCommand("setPhenotype HP:0001283 YES");
		Set<OWLClass> symptom_set = new HashSet<OWLClass>();
		List<OWLClass> symptom_list = new ArrayList<OWLClass>();
		List<Tickbox> ticks = cmd.getPs().getSymptomSuggestions(); 
		for(Tickbox sugg : ticks) {
			OWLClass symp = cmd.getHPOEntry(sugg.getHpoLocalName());
			symptom_list.add(symp);
			symptom_set.add(symp);
		}
		float p = cmd.AveragePrecision(symptom_set, symptom_list);
		assertTrue(p==1.0);
		p = cmd.AveragePrecision(new HashSet<OWLClass>(), symptom_list);
		assertTrue(p==0.0);
		p = cmd.AveragePrecision(symptom_set, new ArrayList<OWLClass>());
		assertTrue(p==0.0);
		p = cmd.AveragePrecision(symptom_set, symptom_list.subList(0, 1));
		assertTrue((p - 1.0/(float) symptom_list.size()) < 0.001);
		Set<OWLClass> singleton = new HashSet<OWLClass>();
		singleton.add(symptom_set.iterator().next());
		p = cmd.AveragePrecision(singleton, symptom_list);
		//assertTrue((p - 1.0/(float) symptom_list.size()) < 0.001);
	}

	
	@Test
	public void testRecall() throws IllegalArgumentException, OWLOntologyStorageException, IOException, HPOException {
		cmd.receiveCommand("setPhenotype HP:0001283 YES");
		Set<OWLClass> symptom_set = new HashSet<OWLClass>();
		List<OWLClass> symptom_list = new ArrayList<OWLClass>();
		List<Tickbox> ticks = cmd.getPs().getSymptomSuggestions(); 
		for(Tickbox sugg : ticks) {
			OWLClass symp = cmd.getHPOEntry(sugg.getHpoLocalName());
			symptom_list.add(symp);
			symptom_set.add(symp);
		}
		float p = cmd.recall(symptom_set, symptom_list);
		assertTrue(p==1.0);
		p = cmd.recall(new HashSet<OWLClass>(), symptom_list);
		assertTrue(p==1.0);
		p = cmd.recall(symptom_set, new ArrayList<OWLClass>());
		assertTrue(p==0.0);
		p = cmd.recall(symptom_set, symptom_list.subList(0, 1));
		assertTrue((p - 1.0/(float) symptom_set.size()) < 0.001);
		Set<OWLClass> singleton = new HashSet<OWLClass>();
		singleton.add(symptom_set.iterator().next());
		p = cmd.recall(singleton, symptom_list);
		assertTrue(p==1.0);
	}
	
	@Test
	public void testGetDiseases() throws IllegalArgumentException, OWLOntologyStorageException, IOException, HPOException {
		cmd.receiveCommand("setPhenotype HP:0001283 YES");
		cmd.receiveCommand("setPhenotype HP:0001511 YES");
		String dis = cmd.receiveCommand("getDiseases");
		assertTrue(dis.contains("Peters plus syndrome"));
	}


	
	@Test
	public void testSubclassOrder() throws OWLOntologyStorageException, IOException, HPOException {
		PhenotypeTree doc_phenos = cmd.readMMIFile("src/test/resources/paper1.mmi");
		assertTrue(doc_phenos.getSize() > 1);
		List<OWLClass> doc_hpcls = cmd.getSubclassOrder(doc_phenos);
		assertTrue(doc_hpcls.size() > 1);
		assertTrue(doc_hpcls.size() == doc_phenos.getSize());
	}
	
	@Test
	public void testExperimentCode() throws OWLOntologyStorageException, IOException, HPOException {
		//http://downloads.hindawi.com/journals/crig/2020/3256539.xml
		//List<Float> precs = cmd.experiment("C1527231", "src/test/resources/paper1.mmi");
		PhenotypeTree doc_phenos = cmd.readMMIFile("src/test/resources/5957415.mmi");
		List<OWLClass> doc_hpcls = cmd.getSubclassOrder(doc_phenos);

		UMLS_MRCONSO_FileReader umls = UMLS_MRCONSO_FileReader.getInstance();
		String omim = umls.getOMIMCode("C0265482");
		HPAnnotationsFileReader annots = HPAnnotationsFileReader.getInstance();
		Set<String> symptoms =  annots.getDiseaseAnnotations(omim);
		Set<OWLClass> symptom_set = new HashSet<OWLClass>();
		for(String symptom : symptoms) {
			symptom_set.add(cmd.getHPOEntry(symptom));
			System.out.println("Symptom: " + symptom);
		}

		List<Float> precs = new ArrayList<Float>();
		for(int i = 1; i < doc_hpcls.size(); i++) {
			System.out.println(cmd.precision(symptom_set, doc_hpcls.subList(0, i)));
		}
		
	}
	
	@Test
	public void testExperiment() throws OWLOntologyStorageException, IOException, HPOException {
		//http://downloads.hindawi.com/journals/crig/2020/3256539.xml
		List<Float> precs = cmd.experiment("C0265482", "src/test/resources/5957415.mmi", 10);
		System.out.println(precs.get(0));
		System.out.println(precs.get(precs.size()-1));
		List<Float> precs1 = cmd.experiment("C1527231", "src/test/resources/3256539.mmi", 10);
		System.out.println(precs1.get(0));
		System.out.println(precs1.get(precs1.size()-1));
		List<Float> precs2 = cmd.experiment("C0265482", "src/test/resources/5957415.mmi", 10);
		System.out.println(precs2.get(0));
		System.out.println(precs2.get(precs2.size()-1));
		List<Float> precs3 = cmd.experiment("C3502510", "src/test/resources/6143050.mmi", 10);
		System.out.println(precs3.get(0));
		System.out.println(precs3.get(precs3.size()-1));
		List<Float> precs4 = cmd.experiment("C4225334", "src/test/resources/8414857.mmi", 10);
		System.out.println(precs4.get(0));
		System.out.println(precs4.get(precs4.size()-1));
	}
	
}
