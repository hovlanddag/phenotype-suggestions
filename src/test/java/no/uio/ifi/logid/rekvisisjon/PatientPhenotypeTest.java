package no.uio.ifi.logid.rekvisisjon;

import static org.junit.Assert.*;

import java.io.IOException;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import no.uio.ifi.logid.rekvisisjon.PatientPhenotype;
import no.uio.ifi.logid.rekvisisjon.PatientPhenotypeFileReader;
import no.uio.ifi.logid.rekvisisjon.PatientPhenotypeReader;
import no.uio.ifi.logid.rekvisisjon.PhenotypeStatement;
import no.uio.ifi.logid.rekvisisjon.hpo.HPOException;

public class PatientPhenotypeTest {

	private PatientPhenotypeReader pr;
	private PatientPhenotype p;
	private static String hpo_code = "HP:0000162";
	private static String config_path = "src/test/resources";
	
	
	
	@Before
	public void setup() throws IOException{
		this.pr = PatientPhenotypeFileReader.getInstance(config_path);
		this.p = pr.createNew("test_patient");
	}
	
	@Test
	public void testAddPositivePhenotype() throws HPOException {
		assertTrue(p.getPhenotypeStatement(hpo_code) == PhenotypeStatement.UNKNOWN);
		p.addPositivePhenotype(hpo_code);
		assertTrue(p.getPhenotypeStatement(hpo_code) == PhenotypeStatement.POSITIVE);
	}
/**
 * Complement class does not work as intended
	@Test
	public void testAddNegativePhenotype() throws HPOException {
		assertTrue(p.getPhenotypeStatement(hpo_code) == PhenotypeStatement.UNKNOWN);
		p.addNegativePhenotype(hpo_code);
		assertTrue(p.getPhenotypeStatement(hpo_code) == PhenotypeStatement.NEGATIVE);
	}
**/
	
	@Test
	public void testRemovePositivePhenotype() throws HPOException {
		assertTrue(p.getPhenotypeStatement(hpo_code) == PhenotypeStatement.UNKNOWN);
		p.addPositivePhenotype(hpo_code);
		assertTrue(p.getPhenotypeStatement(hpo_code) == PhenotypeStatement.POSITIVE);
		p.removePositivePhenotype(hpo_code);
		assertTrue(p.getPhenotypeStatement(hpo_code) == PhenotypeStatement.UNKNOWN);
	}
/**
	@Test
	public void testRemoveNegativePhenotype() throws HPOException {
		assertTrue(p.getPhenotypeStatement(hpo_code) == PhenotypeStatement.UNKNOWN);
		p.addNegativePhenotype(hpo_code);
		assertTrue(p.getPhenotypeStatement(hpo_code) == PhenotypeStatement.NEGATIVE);
		p.removeNegativePhenotype(hpo_code);
		assertTrue(p.getPhenotypeStatement(hpo_code) == PhenotypeStatement.UNKNOWN);
	}
**/
}
