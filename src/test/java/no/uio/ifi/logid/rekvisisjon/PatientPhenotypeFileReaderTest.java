package no.uio.ifi.logid.rekvisisjon;

import java.io.IOException;

import org.junit.Test;

public class PatientPhenotypeFileReaderTest {
	private static String config_file_path="src/test/resources/";
	private static String patient_id = "testPatient";
	
	@Test
	public void testGetInstance() {
		PatientPhenotypeFileReader.getInstance(config_file_path);
	}

/*	@Test
	public void testReadFile() throws IOException {
		PatientPhenotypeReader pr = PatientPhenotypeReader.getInstance(config_file_path);
		pr.createNew(patient_id);
		PatientPhenotype pp = pr.readFile(patient_id);
		assertTrue(pp != null);
	}
*/
	@Test
	public void testReadDataset() {
		PatientPhenotypeReader pr = PatientPhenotypeFileReader.getInstance(config_file_path);
		pr.read(patient_id);
	}

	@Test
	public void testCreateNew() throws IOException {
		PatientPhenotypeReader pr = PatientPhenotypeFileReader.getInstance(config_file_path);
		pr.createNew(patient_id);
	}

}
