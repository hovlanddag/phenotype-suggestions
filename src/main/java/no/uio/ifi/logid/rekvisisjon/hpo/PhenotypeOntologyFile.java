package no.uio.ifi.logid.rekvisisjon.hpo;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.Serializable;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.apache.jena.ontology.OntClass;
import org.apache.jena.ontology.OntModel;
import org.apache.jena.ontology.OntModelSpec;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.Statement;
import org.semanticweb.owlapi.model.IRI;
import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLOntologyStorageException;
import org.semanticweb.owlapi.reasoner.NodeSet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PhenotypeOntologyFile implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4023160176962919291L;
	private static Logger LOG = LoggerFactory.getLogger(PhenotypeOntologyFile.class);
	private static PhenotypeOntologyFile instance = null;

	public static String HPODATABASENAME = "hpo-database" ;
	private static String HP_ONTO_FILENAME = "hp.owl";
	private static String HP_FILE_URL = "http://purl.obolibrary.org/obo/hp.owl";
	
	
	String database_dir;

	private OntModel m;

	private PhenotypeOntologyFile(String config_path) throws IOException {
		Path hp_path = FileSystems.getDefault().getPath(config_path, HP_ONTO_FILENAME);
		this.m = ModelFactory.createOntologyModel(OntModelSpec.OWL_MEM);
		BufferedReader reader = null;
		try {
			reader = Files.newBufferedReader(hp_path);
			//m.read(reader, "");
			m.read(HP_FILE_URL);
			reader.close();
		} catch (IOException e){
			LOG.error("Error reading from ontology file at " + hp_path.toString() + "Cause: " + e.getMessage());
			throw new IOException("Error reading from ontology file at " + hp_path.toString(), e);
		} finally {
			if(reader != null)
				reader.close();
		}
		m.createProperty(HPUtils.hasPhenotypeURI);
	}


	
	

	/**
	 * Assumes input is localname in ontology, e.g. HP_001622
	 * @param hpolocalname
	 * @return
	 * @throws HPOException 
	 */
	public List<Resource> getSubclasses(String hpolocalname, boolean direct) throws HPOException{
		assert(hpolocalname.substring(0,3).equals("HP_") || hpolocalname.substring(0,3).equals("HP:"));
		OntClass superclass = HPUtils.getOntClass(hpolocalname, m);
		assert(superclass != null);
		Iterator<OntClass> iter = superclass.listSubClasses(direct);
		List<Resource> subcls = new ArrayList<Resource>();
		while(iter.hasNext())
			subcls.add(iter.next());
		return subcls;
	}
	

	private List<Tickbox> getTickboxFromClasses(String parent, Iterator<OntClass> subs) throws HPOException{
		List<Tickbox> hps = new ArrayList<Tickbox>();

		Property owoid = m.getProperty("http://www.geneontology.org/formats/oboInOwl#id");
		assert(owoid != null);
		Property rdfslabel = m.getProperty("http://www.w3.org/2000/01/rdf-schema#label");
		assert(rdfslabel != null);

		//for(OntClass cls : subs){
		while(subs.hasNext()){
			OntClass cls = subs.next();
			Statement labelstmt = cls.getRequiredProperty(rdfslabel);
			String label = cls.getLocalName(); 
			if(labelstmt != null)
				label = labelstmt.getString();
			Statement obocodestmt = cls.getProperty(owoid);
			String hpo_localname = cls.getLocalName();
			if(obocodestmt != null)
				hpo_localname = obocodestmt.getString();
			boolean hasSubclasses = cls.hasSubClass();
			Tickbox hpe = new Tickbox(label, parent, hpo_localname, "", hasSubclasses);
			hps.add(hpe);
		}
		return hps;
	}
	
	
	public List<Tickbox> getSubHPOEntries(String parent, String hpocode, boolean direct) throws HPOException{
		//List<OntClass> subs = this.getSubclasses(hpocode, direct);
		OntClass superclass = HPUtils.getOntClass(hpocode, m);
		assert(superclass != null);
		Iterator<OntClass> iter = superclass.listSubClasses(direct);
		return this.getTickboxFromClasses(parent, iter);
	}


	
	public static synchronized PhenotypeOntologyFile getInstance(String config_path) throws IOException {
		if(instance == null){
			instance = new PhenotypeOntologyFile(config_path);
		}
		return instance;
	}

	public OntModel getModel(){
		return this.m;
	}

	static PhenotypeOntologyFile getInstance() {
		assert(instance != null);
		return instance;
	}

	public OntClass getOntClass(String hpo_localname) throws HPOException {
		assert(hpo_localname != null);
		OntClass cls = HPUtils.getOntClass(hpo_localname, this.m);
		return cls;
	}



	public List<Tickbox> getTopClassTickboxes(boolean direct) throws HPOException {
		String owlthing = "HP_0000118";
		return this.getSubHPOEntries("owlthing", owlthing, true);
	}



	public boolean hasSubclass(Resource hpoclass) {
		assert(hpoclass.canAs(OntClass.class));
		hpoclass.as(OntClass.class);
		return ((OntClass) hpoclass).hasSubClass();
	}



}
