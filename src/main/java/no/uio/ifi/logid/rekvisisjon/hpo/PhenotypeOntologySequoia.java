package no.uio.ifi.logid.rekvisisjon.hpo;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.Serializable;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Stream;

import org.apache.jena.rdf.model.Resource;
import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.AxiomType;
import org.semanticweb.owlapi.model.IRI;
import org.semanticweb.owlapi.model.OWLAnnotation;
import org.semanticweb.owlapi.model.OWLAnnotationProperty;
import org.semanticweb.owlapi.model.OWLAxiom;
import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLDataFactory;
import org.semanticweb.owlapi.model.OWLDataProperty;
import org.semanticweb.owlapi.model.OWLLiteral;
import org.semanticweb.owlapi.model.OWLObjectSomeValuesFrom;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import org.semanticweb.owlapi.model.OWLOntologyManager;
import org.semanticweb.owlapi.model.OWLOntologyStorageException;
import org.semanticweb.owlapi.model.SWRLRule;
import org.semanticweb.owlapi.model.parameters.ChangeApplied;
import org.semanticweb.owlapi.model.parameters.Imports;
import org.semanticweb.owlapi.reasoner.InferenceType;
import org.semanticweb.owlapi.reasoner.Node;
import org.semanticweb.owlapi.reasoner.NodeSet;
import org.semanticweb.owlapi.reasoner.OWLReasoner;
import org.semanticweb.owlapi.reasoner.impl.DefaultNodeSet;
import org.semanticweb.owlapi.reasoner.impl.OWLClassNode;
import org.semanticweb.owlapi.reasoner.impl.OWLClassNodeSet;
import org.semanticweb.owlapi.search.EntitySearcher;
import org.semanticweb.owlapi.util.OWLOntologyWalker;
import org.semanticweb.owlapi.util.OWLOntologyWalkerVisitor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sequoiareasoner.owlapi.SequoiaReasonerFactory;



public class PhenotypeOntologySequoia implements Serializable, PhenotypeOntology {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4023160176962919291L;
	private static Logger LOG = LoggerFactory.getLogger(PhenotypeOntologySequoia.class);
	private static PhenotypeOntologySequoia instance = null;

	private static OWLReasoner reasoner = null;
	private static OWLOntologyManager manager;
	private static OWLDataFactory  dfactory;
	private static OWLOntology ontology;

	private static Map<String, OWLClass> XREF_inverse_map;

	public static String HPODATABASENAME = "hpo-database" ;
	private static String HP_ONTO_URI = "http://purl.obolibrary.org/obo/hp.owl#";
	//private static String patientIRIs = "http://ifi.uio.no/rekvisisjon/data/patient#";
	//private static String patientNegIRIs = "http://ifi.uio.no/rekvisisjon/data/patientNeg#";
	private static String HP_ONTO_FILENAME = "hp.ttl";
	//private static DataStore store;
	private static String patientClassStr = "http://ifi.uio.no/rekvisisjon/patient";



	private HashMap<String, List<Tickbox>> allSubclassCache; // Stores inferred subclasses 
	private HashMap<String, List<Tickbox>> directSubclassCache; // Stores direct subclasses
	private static OWLClass patientClass;
	private static Path hp_path;

	private PhenotypeOntologySequoia(String config_path) throws IOException {
		if(manager == null)
			firstTimeSetup(config_path);
		this.allSubclassCache = new HashMap<String, List<Tickbox>>();
		directSubclassCache = new HashMap<String, List<Tickbox>>();

	}

	/**
	 * Loads hp.ttl and keeps in-memory materialized in a local RDFOx database. 
	 * 
	 * @param config_path
	 * @throws IOException 
	 */
	public synchronized static void firstTimeSetup(String config_path) throws IOException{
		hp_path = FileSystems.getDefault().getPath(config_path, HP_ONTO_FILENAME);
		//Location hpo_database_dir = FileSystems.getDefault().getPath(config_path, "hpo-database");
		manager = OWLManager.createOWLOntologyManager();
		dfactory = manager.getOWLDataFactory();
		try {
			System.out.println(hp_path);
			File file = new File(hp_path.toString());
			OWLOntology rule_ontology = manager.loadOntologyFromOntologyDocument(file);
			Set<OWLAxiom> axioms = rule_ontology.getAxioms();
			ontology = manager.createOntology();
			Set<OWLAxiom> newaxioms = new HashSet<OWLAxiom>();
			for(OWLAxiom rule : axioms){
				if(! rule.isOfType(AxiomType.SWRL_RULE) && ! rule.isOfType(AxiomType.CLASS_ASSERTION))
					newaxioms.add(rule);
			}

			ChangeApplied retval = manager.addAxioms(ontology, newaxioms);
			if(retval != ChangeApplied.SUCCESSFULLY){
				throw new IOException("Unsuccessful ontology creation: return value when adding axioms: " + retval.toString());
			}
			Set<SWRLRule> rules = ontology.getAxioms(AxiomType.SWRL_RULE);
			assert(rules.isEmpty());

			reasoner=new SequoiaReasonerFactory().createNonBufferingReasoner(ontology);

			reasoner.precomputeInferences(InferenceType.CLASS_HIERARCHY);
		} catch (OWLOntologyCreationException e) {
			LOG.error("Error reading from ontology file at " + hp_path.toString() + "Cause: " + e.getMessage());
			throw new IOException("Error reading from ontology file at " + hp_path.toString(), e);
		}



	}


	/**
	 * Assumes input is localname in ontology, e.g. HP_001622
	 * @param hpolocalname
	 * @return
	 * @throws HPOException 
	 */
	public NodeSet<OWLClass> getSubclasses(String hpolocalname, boolean direct) throws IllegalArgumentException{
		assert(hpolocalname.substring(0,3).equals("HP_"));
		String phenouri = HPUtils.getHPuri(hpolocalname);
		IRI phenoiri = IRI.create(phenouri);
		OWLClass phenocls = dfactory.getOWLClass(phenoiri); 
		NodeSet<OWLClass> subcls = reasoner.getSubClasses(phenocls, direct);
		return subcls;
	}

	/**
	 * Assumes input is localname in ontology, e.g. HP_001622
	 * @param hpolocalname
	 * @return
	 * @throws HPOException 
	 */
	@Override
	public List<Tickbox> getSubHPOEntries(String hpo_parent, String hpocode, boolean direct) throws IllegalArgumentException{
		NodeSet<OWLClass> subcls = getSubclasses(hpocode, direct);
		return this.codeToTickbox(subcls, hpo_parent, true);
	}

	@Override
	public List<Tickbox> codeToTickbox(List<String> cls_list, boolean navigate) throws HPOException{
		DefaultNodeSet<OWLClass> cls_set = new OWLClassNodeSet();
		for(String hpolocalname : cls_list){
			String phenouri = hpolocalname;
			if(hpolocalname.substring(0, 2).equals("HP"))
				phenouri = HPUtils.getHPuri(hpolocalname);
			IRI phenoiri = IRI.create(phenouri);
			OWLClass phenocls = dfactory.getOWLClass(phenoiri); 
			cls_set.addNode(new OWLClassNode(phenocls));
		}
		return this.codeToTickbox(cls_set, "Selections", navigate);
	}

	@Override
	public List<Tickbox> iriToTickbox(List<IRI> cls_list, boolean navigate) throws HPOException{
		DefaultNodeSet<OWLClass> cls_set = new OWLClassNodeSet();
		for(IRI phenoiri : cls_list){
			OWLClass phenocls = dfactory.getOWLClass(phenoiri); 
			cls_set.addNode(new OWLClassNode(phenocls));
		}
		return this.codeToTickbox(cls_set, "Selections", navigate);
	}

	@Override
	public List<Tickbox> codeToTickbox(NodeSet<OWLClass> cls_list, String hpo_parent, boolean navigate) throws IllegalArgumentException {

		List<Tickbox> hps = new ArrayList<Tickbox>();

		String owoid = "http://www.geneontology.org/formats/oboInOwl#id";
		assert(owoid != null);
		String rdfslabel = "http://www.w3.org/2000/01/rdf-schema#label";
		//IRI labeliri = IRI.create(rdfslabel);
		//OWLDataProperty labelprop = dfactory.getOWLDataProperty(labeliri);
		assert(rdfslabel != null);


		for(Node<OWLClass> node : cls_list){
			OWLClass cls = node.getRepresentativeElement();
			String label = "?";
			String comment = "";
			for (OWLAnnotation annotation : EntitySearcher.getAnnotations(cls, ontology, dfactory.getRDFSLabel())) {
				if (annotation.getValue() instanceof OWLLiteral) {
					OWLLiteral val = (OWLLiteral) annotation.getValue();
					label = val.getLiteral();
					break;
				}
			}
			for (OWLAnnotation annotation : EntitySearcher.getAnnotations(cls, ontology, dfactory.getRDFSComment())) {
				if (annotation.getValue() instanceof OWLLiteral) {
					OWLLiteral val = (OWLLiteral) annotation.getValue();
					comment = val.getLiteral();
					break;
				}
			}
			String childiri = cls.getIRI().toString();
			if(cls.isBottomEntity())
				continue;
			int hpocode_length = childiri.lastIndexOf('/');
			String child_code = childiri.substring(hpocode_length+1);
			if(!child_code.substring(0, 3).equals("HP_"))
				System.out.println(child_code);
			assert(child_code.substring(0, 3).equals("HP_"));

			boolean hasSubclasses = navigate && this.hasSubclass(child_code);



			Tickbox hpe = new Tickbox(label, hpo_parent, child_code, comment, hasSubclasses);
			hps.add(hpe);
		}
		return hps;
	}


	@Override
	public boolean hasSubclass(String hpo_code) throws IllegalArgumentException{
		NodeSet<OWLClass> subsubs = getSubclasses(hpo_code, false);
		if(subsubs.isEmpty())
			return false;
		if(subsubs.isSingleton())
			return !subsubs.isBottomSingleton();
		return true;
	}
	/**
	 * Assumes input is localname in ontology, e.g. HP_001622
	 * @param direct 
	 * @param hpolocalname
	 * @return
	 * @throws HPOException 
	 */
	public List<Tickbox> getDirectSubHPOEntries(String hpo_parent, String hpocode) throws IllegalArgumentException{
		if(directSubclassCache.containsKey(hpocode)){
			return directSubclassCache.get(hpocode);
		}

		List<Tickbox> hps = this.getSubHPOEntries(hpo_parent, hpocode, true);

		directSubclassCache.put(hpocode, hps);
		return hps;
	}



	/**
	 * Assumes input is localname in ontology, e.g. HP_001622
	 * @param hpolocalname
	 * @return
	 * @throws HPOException 
	 */
	public List<Tickbox> getAllSubHPOEntries(String hpo_parent, String hpocode) throws IllegalArgumentException{
		if(allSubclassCache.containsKey(hpocode)){
			return allSubclassCache.get(hpocode);
		}
		List<Tickbox> hps = this.getSubHPOEntries(hpo_parent, hpocode, false);		
		allSubclassCache.put(hpocode, hps);
		return hps;
	}


	public static synchronized PhenotypeOntologySequoia getInstance(String config_path) throws IOException {
		if(instance == null){
			instance = new PhenotypeOntologySequoia(config_path);
		}
		return instance;
	}



	public static PhenotypeOntologySequoia getInstance() {
		assert(instance != null);
		return instance;
	}






	@Override
	public List<Tickbox> getTopClassTickboxes(boolean direct) throws IllegalArgumentException {
		String owlthing = "HP_0000118";
		return this.getDirectSubHPOEntries("owlthing", owlthing);

	}
	@Override
	public boolean consistent(String patientid) throws HPOException {
		return reasoner.isConsistent();
	}

	private synchronized void saveOntology() throws OWLOntologyStorageException, IOException{

		File file = new File(hp_path.toString());
		FileOutputStream os = new FileOutputStream(file);
		manager.saveOntology(ontology, os);
		os.close();
	}



	@Override
	public List<Tickbox> getPatientIDList() {
		return new ArrayList<Tickbox>();
	}



	
	public OWLClass getOntClass(String hpo_localname) {
		String phenouri = HPUtils.getHPuri(hpo_localname);
		IRI phenoiri = IRI.create(phenouri);
		OWLClass phenocls = dfactory.getOWLClass(phenoiri);
		return phenocls;

	}

	@Override
	public boolean hasSubclass(Resource hpoclass) {
		// TODO Auto-generated method stub
		return false;
	}
	@Override
	public NodeSet<OWLClass> getSubClasses(OWLClass phenocls, boolean b) {
		return reasoner.getSubClasses(phenocls, b);

	}

	@Override
	public NodeSet<OWLClass> getSuperClasses(OWLClass phenocls, boolean b) {
		return  reasoner.getSuperClasses(phenocls, b);

	}

	/**
	 * Gets the hpo class referenced by the input UMLS entry, if any. 
	 * Input format "UMLS:C123446"
	 * Looks for annotation property oboInOwl:hasDbXref entries in the ontology
	 * http://www.geneontology.org/formats/oboInOwl#ohasDbXref
	 */
	@Override
	public OWLClass getUMLSEntry(String umls_code) {
		if(XREF_inverse_map == null)
			createXREFLookup();
		return XREF_inverse_map.get(umls_code);
	}

	/**
	 * Instantiates inverse map of XREF entries in HPO. Used for UMLS to HPO mapping
	 */
	public synchronized void createXREFLookup() {
		if(XREF_inverse_map == null) {
			XREF_inverse_map = new HashMap<String, OWLClass>();
			OWLAnnotationProperty xref_prop_name = manager.getOWLDataFactory().
					getOWLAnnotationProperty(
							IRI.create("http://www.geneontology.org/formats/oboInOwl#hasDbXref"));
			for (OWLClass cls :  this.ontology.getClassesInSignature()) {
				for (OWLAnnotation annotation :  EntitySearcher
						.getAnnotationObjects(cls, PhenotypeOntologySequoia.ontology)) {
					OWLAnnotationProperty ann_prop = annotation.getProperty();
					if(ann_prop.equals(xref_prop_name)) {
						OWLLiteral val = (OWLLiteral) annotation.getValue();
						XREF_inverse_map.put(val.getLiteral(), cls);
					}
				}
			}
		}
	}

	@Override
	public boolean isSuperClass(OWLClass sup, OWLClass child) {
		NodeSet<OWLClass> sup_off_child = reasoner.getSuperClasses(child, false);
		return sup_off_child.containsEntity(sup);
		
	}

}

