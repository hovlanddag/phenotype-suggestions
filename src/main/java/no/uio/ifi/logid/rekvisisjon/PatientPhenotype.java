package no.uio.ifi.logid.rekvisisjon;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.apache.jena.ontology.ComplementClass;
import org.apache.jena.ontology.Individual;
import org.apache.jena.ontology.OntClass;
import org.apache.jena.ontology.OntModel;
import org.apache.jena.query.Dataset;
import org.apache.jena.query.ReadWrite;
import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.RDFNode;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.Statement;
import org.apache.jena.reasoner.ValidityReport;

import no.uio.ifi.logid.rekvisisjon.hpo.HPAnnotation;
import no.uio.ifi.logid.rekvisisjon.hpo.HPAnnotationsFileReader;
import no.uio.ifi.logid.rekvisisjon.hpo.HPOException;
import no.uio.ifi.logid.rekvisisjon.hpo.HPUtils;

public class PatientPhenotype {
	
	private Individual patient;
	private OntModel model = null;
	private Dataset ds = null;
	
	public PatientPhenotype(OntModel model, Dataset ds, Individual patient){
		this.model = model;
		this.patient = patient;
		this.ds = ds;
		assert(model.containsResource(patient));
	}
	
	private Statement getPositivePhenotype(String hpo_localname) throws HPOException{
		if(this.ds != null)
			this.ds.begin(ReadWrite.READ);
		OntClass cl = HPUtils.getOntClass(hpo_localname, this.model);
		Property is_a = model.getProperty(HPUtils.hasPhenotypeURI);
		if(this.ds != null)
			this.ds.end();
		return model.createStatement(patient, is_a, cl);
	}
	
	private Statement getNegativePhenotype(String hpo_localname) throws HPOException{
		assert(patient != null);
		assert(model != null);
		String cl_uri = HPUtils.getHPuri(hpo_localname);
		assert(cl_uri != null);
		OntClass cls = model.getOntClass(cl_uri);
		assert(cls != null);
		ComplementClass neg_cl = model.createComplementClass(null, cls);
		assert(neg_cl != null);
		assert(neg_cl.getOperand().equals(cls));
		Property is_a = model.getProperty(HPUtils.hasPhenotypeURI);
		assert(is_a != null);
		return model.createStatement(patient, is_a, neg_cl);
	}
	
	public void addPositivePhenotype(String hpo_localname) throws HPOException{
		Statement stmt = this.getPositivePhenotype(hpo_localname);
		Statement neg_stmt = this.getNegativePhenotype(hpo_localname);
		assert(!model.contains(stmt));
		assert(!model.contains(neg_stmt));
		/*if(model.contains(neg_stmt))
			model.remove(neg_stmt);*/
		model.add(stmt);
	}
	
	/**
	 * Complement Class generates new class at each call, so does not work as intended
	public void addNegativePhenotype(String hpo_localname) throws HPOException{
		Statement stmt = this.getPositivePhenotype(hpo_localname);
		Statement neg_stmt = this.getNegativePhenotype(hpo_localname);
		assert(!model.contains(neg_stmt));
		//assert(!model.contains(stmt));
		if(model.contains(stmt))
			model.remove(stmt);
		model.add(neg_stmt);
	
	}
	**/
	
	public void removePositivePhenotype(String hpo_localname) throws HPOException{
		Statement stmt = this.getPositivePhenotype(hpo_localname);
		assert(model.contains(stmt));
		model.remove(stmt);
		assert(!model.contains(stmt));
	}
	
	/**
	public void removeNegativePhenotype(String hpo_localname) throws HPOException{
		Statement neg_stmt = this.getNegativePhenotype(hpo_localname);
		assert(model.contains(neg_stmt));
		model.remove(neg_stmt);
		assert(!model.contains(neg_stmt));
	}
	**/
	/**
	 * Called when clicking "N/A" in the web interface
	 * @param hpo_localname
	 * @throws HPOException
	 */
	public void removePhenotype(String hpo_localname) throws HPOException{
		Statement stmt = this.getPositivePhenotype(hpo_localname);
		if(model.contains(stmt))
			model.remove(stmt);
		assert(!model.contains(stmt));
		Statement neg_stmt = this.getNegativePhenotype(hpo_localname);
		if(model.contains(neg_stmt))
			model.remove(neg_stmt);
		assert(!model.contains(neg_stmt));
	}
	
	/**
	 * The assertion of the HPO code give
	 * @throws HPOException 
	 */
	public PhenotypeStatement getPhenotypeStatement(String hpo_localname) throws HPOException{
		Statement stmt = this.getPositivePhenotype(hpo_localname);
		Statement neg_stmt = this.getNegativePhenotype(hpo_localname);
		if(model.contains(neg_stmt)){
			assert(!model.contains(stmt));
			return PhenotypeStatement.NEGATIVE;
		} else if (model.contains(stmt)){
			assert(!model.contains(neg_stmt));
			return PhenotypeStatement.POSITIVE;
		} else {
			return PhenotypeStatement.UNKNOWN;
		}
	}
	
	/**
	 * Gets all annotations of all hp classes the patient is of
	 * @return
	 */
	public Set<HPAnnotation> getAnnotations(){
		Set<HPAnnotation> annots = new HashSet<HPAnnotation>();
		HPAnnotationsFileReader hpfr = HPAnnotationsFileReader.getInstance();
		Property is_a = this.model.getProperty(HPUtils.hasPhenotypeURI);
		Iterator<RDFNode> iter = model.listObjectsOfProperty(patient, is_a);
		while(iter.hasNext()){
			Resource r = iter.next().asResource();
			String hpocode = r.getLocalName();
			if(hpocode != null){
				Set<HPAnnotation> l = hpfr.getAnnotations(hpocode);
				if(l != null)
					annots.addAll(l);
			}
		}
		return annots; 
		
	}
	
	/**
	 * Gets all hpo codes the patient is seen to have as a trait
	 * @return
	 */
	public List<String> getHPOs(){
		List<String> annots = new ArrayList<String>();
		Property is_a = this.model.getProperty(HPUtils.hasPhenotypeURI);
		Iterator<RDFNode> iter = model.listObjectsOfProperty(patient, is_a);
		while(iter.hasNext()){
			RDFNode n = iter.next();
			Resource r = n.asResource();
			if(r.getLocalName() != null)
				annots.add(r.getLocalName());
		}
		return annots; 
		
	}

	public ValidityReport validate() {
		return model.validate();
	}
	
	
	
}

