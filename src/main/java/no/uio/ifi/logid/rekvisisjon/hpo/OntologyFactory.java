package no.uio.ifi.logid.rekvisisjon.hpo;

import java.io.IOException;
import java.util.concurrent.Semaphore;

import org.apache.http.auth.AuthScope;
import org.apache.http.auth.Credentials;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.HttpClient;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.HttpClients;
import org.apache.jena.riot.web.HttpOp;

public class OntologyFactory  {

	private String config_path;
	public enum backend_name {
		FILE, ARQ, TDB, RDFOx, Sequoia
	}
	private backend_name backend;
	private static OntologyFactory instance;
	private static Semaphore factory_lock = new Semaphore(1);
	private PhenotypeOntology onto;

	private OntologyFactory(String config_path, backend_name backend) throws IOException {
		super();
		this.backend = backend;
		this.config_path = config_path;
	}
	
	private void setupNoARQ() throws IOException{
		if(backend == backend_name.Sequoia)
			onto = PhenotypeOntologySequoia.getInstance(config_path);
		else
			assert(false);
	}
		
	public static void init(String config_path, backend_name backend) throws IOException, InterruptedException{
		factory_lock.acquire();
		if(instance == null){
			instance = new OntologyFactory(config_path, backend);
			instance.setupNoARQ();
		}
		factory_lock.release();

	}


	public static PhenotypeOntology getOntology() throws IOException {
		assert(instance != null);
		assert(instance.onto != null);
		return instance.onto;
	}

}
