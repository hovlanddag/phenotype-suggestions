package no.uio.ifi.logid.rekvisisjon.hpo;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import no.uio.ifi.logid.rekvisisjon.CSVUtils;
import no.uio.ifi.logid.rekvisisjon.hpo.HPAnnotation.database_name;

/**
 * Reads and stores the configuration file for the top level categories shown in the requisition
 * @author dag
 *
 */
public class HPAnnotationsFileReader {


	private HPAnnotationsList list;

	private static final Logger LOG = LoggerFactory.getLogger(HPAnnotationsFileReader.class);

	private static HPAnnotationsFileReader instance = null;


	private HPAnnotationsFileReader(String config_file_path) throws IOException {

		Path list_path = FileSystems.getDefault().getPath(config_file_path, "phenotype_annotation.tab");
		int line_no = 0;
		
		LOG.debug("Reading file from: " + list_path.toString());
		BufferedReader reader = null;
		try {
			reader = Files.newBufferedReader(list_path);
			list = new HPAnnotationsList();
			String line = null;
			while((line = reader.readLine()) != null){
				line_no ++;
				List<String> parsedline = CSVUtils.parseLine(line, '\t');
				if(parsedline.isEmpty()){
					throw new IOException("Line " + line_no + " is empty in file " + list_path.toString());
				}
				if(parsedline.get(0) == null){
					LOG.error("Line " + line_no + " has empty required database name in file " + list_path.toString());
					continue;
				}
				HPAnnotation.database_name database = HPAnnotation.parseDatabaseName(parsedline.get(0));
				if(parsedline.get(1) == null){
					LOG.error("Line " + line_no + " has empty first column in file " + list_path.toString());
					continue;
				}
				String db_local_id = parsedline.get(1);
				String db_local_name = parsedline.get(2);
				HPAnnotation.qualifier_name qualifier = HPAnnotation.parseQualifier(parsedline.get(3));
				String hpo_localname = parsedline.get(4);
				assert(hpo_localname != null);
				if(hpo_localname.substring(0, 3).equals("HP:")){
					hpo_localname = "HP_" + hpo_localname.substring(3);
				}
				String db_reference = parsedline.get(5);
				String evidence_code = parsedline.get(6);
				list.addAnnotation(hpo_localname, new HPAnnotation(database, db_local_id, findWebLink(database, db_local_id), db_local_name, qualifier, hpo_localname, db_reference, evidence_code));
				list.increaseDiseaseFrequency(db_reference);
			}
			if(line_no == 0)
				throw new IOException("The annotation file \"" + list_path.toString() + "\" is empty");
			
		} catch (IOException e) {
			LOG.error("Error when reading from hpo annotations file at \"" + list_path.toString() + "\": " + e.getMessage());
			throw new IOException("Error when reading from configuration file " + list_path.toString() + " at line " + line_no + ": " + e.getMessage(), e);
		} finally {
			if(reader != null)
				reader.close();
		}
		assert(this.list != null);
		this.list.setAnnotDiseaseFrequencies();
	}

	/**
	 * 
	 * @return Address of web page describing the disease or gene annotated
	 */
	public static String findWebLink(database_name db, String id){
		switch(db){
		case OMIM:
			return "https://www.omim.org/entry/" + id;
		case ORPHA:
			return "http://bioportal.bioontology.org/ontologies/ORDO/?p=classes&conceptid=http%3A%2F%2Fwww.orpha.net%2FORDO%2FOrphanet_" + id;
		case DECIPHER:
			return "";
		default:
			break;
		}
		return "";
	}
	
	public static synchronized HPAnnotationsFileReader getInstance(String configfolder) throws IOException {
		if(HPAnnotationsFileReader.instance == null)
			HPAnnotationsFileReader.instance = new HPAnnotationsFileReader(configfolder);
		assert(instance != null);
		return instance;
	}

	public Set<HPAnnotation> getAnnotations(String hpo_localname){
		if(hpo_localname.substring(2, 3).equals(":"))
			hpo_localname = hpo_localname.replace(':', '_');
		Set<HPAnnotation> retval =  this.list.getAnnotations(hpo_localname);
		if(retval == null)
			retval = new HashSet<HPAnnotation>();
		return retval;
	}

	public static HPAnnotationsFileReader getInstance() {
		assert(instance != null);
		return instance;
	}

	public Set<String> getDiseaseAnnotations(String string) {
		return this.list.getDiseaseAnnotations(string);
	}
}
