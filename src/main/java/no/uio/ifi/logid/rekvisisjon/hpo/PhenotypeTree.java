/**
 * A tree of HPO Classes of phenotypes
 */

package no.uio.ifi.logid.rekvisisjon.hpo;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.semanticweb.owlapi.model.OWLClass;

public class PhenotypeTree {
	private OWLClass hpo_class;
	private List<PhenotypeTree> children;
	
	/**
	 * HP_0000118 is the root of the phenotypic abnormality tree
	 * @return
	 * @throws HPOException
	 * @throws IOException
	 */
	public static PhenotypeTree createRoot() throws HPOException, IOException {
		return new PhenotypeTree(OntologyFactory.getOntology().getOntClass("HP_0000118"));
	}
	
	private PhenotypeTree(OWLClass _hpo_class) {
		this.hpo_class = _hpo_class;
		this.children = new ArrayList<PhenotypeTree>();
	}
	
	
	public PhenotypeTree insert(OWLClass cls) throws IOException {
		PhenotypeOntology onto = OntologyFactory.getOntology();
		if(onto.isSuperClass(cls, this.hpo_class)){
			PhenotypeTree nt = new PhenotypeTree(cls);
			nt.addChild(this);
			return nt;
		} else {
			Iterator<PhenotypeTree> cs = this.getChildren();
			while(cs.hasNext()) {
				PhenotypeTree child = cs.next();
				if(onto.isSuperClass(child.getHpoClass(), cls)) {
					return child.insert(cls);
				}
			}
		}
		this.addChild(new PhenotypeTree(cls));
		return this;
		
	}
	
	public int getSize() {
		int size = 1;
		for(PhenotypeTree t : this.children)
			size++;
		return size;
	}
	
	

	public void addChild(PhenotypeTree phenotypeTree) {
		this.children.add(phenotypeTree);
	}
	
	public OWLClass getHpoClass() { 
		return this.hpo_class;
	}
	
	public Iterator<PhenotypeTree> getChildren(){
		return this.children.iterator();
	}
	
}
