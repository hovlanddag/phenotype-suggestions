package no.uio.ifi.logid.rekvisisjon.hpo;

import java.io.Serializable;

import org.apache.jena.atlas.logging.Log;

import no.uio.ifi.logid.rekvisisjon.ResponseType;
//import org.apache.jena.rdf.model.Resource;


/**
 * Represents a single entry / box to be ticked on the entry form
 * Includes link to HPO entry
 * @author dag
 *
 */
public class Tickbox implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -2875447715074931687L;
	/**
	 * 
	 */
	private String label;
	private String hpoLocalName;
	private String hpoParent;
	private String notes;
	private boolean hasSubclasses;
	private String infopageurl;
	private boolean has_click; // Is Yes or No clicked
	private ResponseType response;
	
	private String yesChecked = ""; // "checked" if yes
	private String noChecked = ""; // "checked" if no
	

	Tickbox(){
		this.label = "None";
		this.hpoLocalName = "0";
		this.notes = "No notes";
		this.has_click = false;
	}
	
	@Override
	public boolean equals(Object t){
		if(t instanceof Tickbox)
			return ((Tickbox) t).getHpoLocalName().equals(this.getHpoLocalName());
		return false;
	}
	
	@Override
	public int hashCode(){
		return this.getHpoLocalName().hashCode();
	}

	public Tickbox(String label, 
			String HPO_parent, 
			String HPO_localname, 
			String notes, 
			boolean hasSubclasses) 
	{
		assert(label != null);
		this.label = label;
		assert(HPO_localname != null);
		this.hpoParent = HPO_parent;
		this.hpoLocalName = HPO_localname;
		this.notes = notes;
		this.hasSubclasses = hasSubclasses;
		try {
			this.infopageurl = HPUtils.getInfoPageURL(HPO_localname);
		} catch (HPOException e) {
			this.infopageurl = "http://google.com/search?" + HPO_localname;
		}
	}

	public Tickbox(String label, String HPO_parent, String hpo_localname, String notes)  {
		assert(label != null);
		this.label = label;
		assert(hpo_localname != null);
		this.hpoParent = HPO_parent;
		this.hpoLocalName = hpo_localname;
		this.notes = notes;
		this.hasSubclasses = false;
		try {
			this.infopageurl = HPUtils.getInfoPageURL(hpo_localname);
		} catch (HPOException e) {
			this.infopageurl = "http://google.com/search?" + hpo_localname;
		}
	}

	public void setResponse(ResponseType res){
		assert(res != null);
		this.response = res;
		this.has_click = true;
		if(res == ResponseType.PRESENT) {
			setYesChecked("checked");
			setNoChecked("");
		} else if (res == ResponseType.NOT_CHECKED) {
			setYesChecked("");
			setNoChecked("");
		} else {
			setNoChecked("checked");
			setYesChecked("");
		}	
	}
	
	/**
	 * Returns null if !this.has_click
	 * @return
	 */
	public ResponseType getResponse(){
		return this.response;
	}
	
	public boolean getHasClick() {
		return this.has_click;
	}
	
	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	
	public String getHpoLocalName() {
		return hpoLocalName;
	}

	public String getHpoParent() {
		return hpoParent;
	}
	
	public void setHpoParent(String hpoParent) {
		this.hpoParent = hpoParent;
	}
	
	public void setHpoLocalName(String hpoLocalName) {
		this.hpoLocalName = hpoLocalName;
	}



	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public boolean getHasSubclasses() {
		return hasSubclasses;
	}

	public void setHasSubclasses(boolean hasSubclasses) {
		this.hasSubclasses = hasSubclasses;
	}


	public String getInfopageurl() throws HPOException {
			return infopageurl;
		
	}

	public void setInfopageurl(String infopageurl) {
		this.infopageurl = infopageurl;
	}

	/**
	 * @return the noChecked
	 */
	public String getNoChecked() {
		return this.noChecked;
	}

	/**
	 * @param noChecked the noChecked to set
	 */
	public void setNoChecked(String _noChecked) {
		this.noChecked = _noChecked;
	}

	/**
	 * @return the yesChecked
	 */
	public String getYesChecked() {
		return this.yesChecked;
	}

	/**
	 * @param yesChecked the yesChecked to set
	 */
	public void setYesChecked(String _yesChecked) {
		this.yesChecked = _yesChecked;
	}




}
