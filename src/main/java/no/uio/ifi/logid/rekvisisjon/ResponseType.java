package no.uio.ifi.logid.rekvisisjon;

public enum ResponseType {
	PRESENT, 
	NOT_PRESENT, 
	// The phenotype has not been checked for
	NOT_CHECKED,
	// Patient too young for phenotype to present
	TOO_YOUNG,
	// Phenotype cannot be present because of other traits of the patient
	CANNOT_PRESENT,
	// For technical (not patient related) reasons, this phenotype cannot be tested for
	CANNOT_CHECK
	
	
	
}
