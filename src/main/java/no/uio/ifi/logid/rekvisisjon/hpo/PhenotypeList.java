package no.uio.ifi.logid.rekvisisjon.hpo;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * Session Bean implementation class PhenotypeList
 */

public class PhenotypeList implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2798609527689375239L;
	Map<String, List<Tickbox>> list = null;
	
	
    public Map<String, List<Tickbox>> getList() {
		return list;
	}


	public void setList(Map<String, List<Tickbox>> list) {
		this.list = list;
	}


	/**
     * Default constructor. 
     */
    public PhenotypeList() {
        // TODO Auto-generated constructor stub
    }


	public PhenotypeList(Map<String, List<Tickbox>> tickboxes) {
		this.list = tickboxes;
	}

}
