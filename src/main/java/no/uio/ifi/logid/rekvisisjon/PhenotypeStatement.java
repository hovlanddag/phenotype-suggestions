package no.uio.ifi.logid.rekvisisjon;

public enum PhenotypeStatement {
	NEGATIVE, POSITIVE, UNKNOWN
}
