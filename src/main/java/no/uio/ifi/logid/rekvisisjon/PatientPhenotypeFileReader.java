package no.uio.ifi.logid.rekvisisjon;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.apache.jena.ontology.Individual;
import org.apache.jena.ontology.OntModel;
import org.apache.jena.rdf.model.ModelFactory;

import no.uio.ifi.logid.rekvisisjon.hpo.HPUtils;

public class PatientPhenotypeFileReader implements PatientPhenotypeReader {
	private String config_file_path;
	private static PatientPhenotypeFileReader instance;
	
	private PatientPhenotypeFileReader(String config_file_path){
		this.config_file_path = config_file_path;
		
	}

	public synchronized static PatientPhenotypeFileReader getInstance(String config_file_path){
		if(instance == null)
			instance = new PatientPhenotypeFileReader(config_file_path);
		return instance;
	}

	
	private String getPatientFile(String patient_id){
		return this.config_file_path + "/patients/" + patient_id;
	}
	
	public PatientPhenotype read(String patient_id){
		if(new File(this.getPatientFile(patient_id)).exists()){
			OntModel model = ModelFactory.createOntologyModel();
			HPUtils.addHPreference(model, this.config_file_path);
			model.read( "file:" + this.getPatientFile(patient_id) );
			Individual patient = HPUtils.getPatientIndividual(patient_id, model);
			assert(patient != null);
			return new PatientPhenotype(model, null, patient);
		} else 
			return null;
	}
	
	public PatientPhenotype createNew(String patient_id) throws IOException{
		OntModel model = ModelFactory.createOntologyModel();
		HPUtils.addHPmodel(model, this.config_file_path);
		model =  HPUtils.createPatient(patient_id, model);
		Individual patient = HPUtils.getPatientIndividual(patient_id, model);
		assert(patient != null);
		
		//model.write( null, "file:" + this.getPatientFile(patient_id) );
		return new PatientPhenotype(model, null, patient);
	}

	@Override
	public List<String> getPatientIDSList() throws IOException {
		// TODO Auto-generated method stub
		return null;
	}
}
