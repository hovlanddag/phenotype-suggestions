package no.uio.ifi.logid.rekvisisjon.hpo;

import java.io.Serializable;
import java.util.Map;
import java.util.Set;

public class Diagnosis implements Comparable<Diagnosis>, Serializable {
	/**
	 * Maintains information about diagnostic information for a disease. 
	 * Identified by the database and database identifier (E.g. OMIM:227220)
	 * Keeps the number of "YES", "NO" and not set hpo entries 
	 * which are annotated with this diseas
	 */
	
	public enum ClickValue {
		YES_CLICKED, NO_CLICKED, YES_INFERRED, NO_INFERRED, UNKNOWN
	}
	
	private Set<String> lacking_hpos;
	
	private HPAnnotation annot;
	
	private Map<String, ClickValue> entry_clicks; // Maps hpo identifier to Click value
	
	public Diagnosis(HPAnnotation annot){
		this.annot = annot;
		this.lacking_hpos = annot.getOther_hpo_ids();
		this.addHPO(this.annot.getHpo_id());
	}
	
	public HPAnnotation getAnnotation(){
		return annot;
	}
	
	public boolean addHPO(String code){
		return this.lacking_hpos.remove(code);
	}
	
	public Set<String> getLackingHpos(){
		return this.lacking_hpos;
	}
	
	/**
	 * The proportion of not selected phenotypes
	 * Returns 'number of not selected symptoms of this disease' divided by 'total number of symptoms/phenotypes'
	 */
	public float getRho(){
		Integer n_lack = this.getLackingHpos().size();
		HPAnnotation annot = this.getAnnotation();
		Integer n_tot = annot.getOther_hpo_ids().size() + 1;
		return n_lack / n_tot;
	}
	
	@Override
	public int compareTo(Diagnosis o) {
		return this.getAnnotation().getDisease_frequency() - o.getAnnotation().getDisease_frequency();
	}
	
	@Override
	public boolean equals(Object o){
		if(o instanceof Diagnosis)
			return this.getAnnotation().getDb_reference() == ((Diagnosis) o).getAnnotation().getDb_reference();
		else
			return false;
	}

	public Map<String, ClickValue> getEntry_clicks() {
		return entry_clicks;
	}

	public void setEntry_clicks(Map<String, ClickValue> entry_clicks) {
		this.entry_clicks = entry_clicks;
	}
}
