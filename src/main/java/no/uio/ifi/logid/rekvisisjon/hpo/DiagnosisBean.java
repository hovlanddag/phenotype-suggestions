package no.uio.ifi.logid.rekvisisjon.hpo;

import java.io.Serializable;
import java.util.Map;
import java.util.Set;

import no.uio.ifi.logid.rekvisisjon.hpo.Diagnosis.ClickValue;
import no.uio.ifi.logid.rekvisisjon.hpo.HPAnnotation.database_name;
import no.uio.ifi.logid.rekvisisjon.hpo.HPAnnotation.qualifier_name;

/**
 * Stores HPO annotation and diagnosis information (priority) in a bean
 * Sending this to the servlet for presentation
 * @author dag
 *
 */

public class DiagnosisBean implements Comparable<DiagnosisBean>, Serializable{

	private static final long serialVersionUID = -91335502289181616L;
	
	private database_name database;
	private String db_name; // Name given in database
	private String db_object_id; // Id in the database, e.g. 154700
	private String db_reference; // OMIM:154700 or PMID:15517394
	private int disease_frequency; // number of entries in which this disease occurs
	private Map<String, ClickValue> entry_clicks; // Maps hpo identifier to Click value
	private String evidence_code; // optional E.g. IEA
	private String hpo_id; // Id in HP. E.g. HP:0002345
	private Set<String> lacking_hpos;
	private Set<String> other_hpo_ids; // The other HPO ids annotated with this disease
	private qualifier_name qualifier; // optional E.g. not
	private String web_link; // Adress of web page about disease
	
	
    /**
     * Default constructor. 
     */
    public DiagnosisBean(Diagnosis d) {
        this.database = d.getAnnotation().getDatabase();
        this.db_name = d.getAnnotation().getDb_name();
        this.db_object_id = d.getAnnotation().getDb_object_id();
        this.db_reference = d.getAnnotation().getDb_reference();
        this.disease_frequency = d.getAnnotation().getDisease_frequency();
        this.evidence_code = d.getAnnotation().getEvidence_code();
        this.hpo_id = d.getAnnotation().getHpo_id();
        this.lacking_hpos = d.getLackingHpos();
        this.other_hpo_ids = d.getAnnotation().getOther_hpo_ids();
        this.qualifier = d.getAnnotation().getQualifier();
        this.web_link = d.getAnnotation().getWeb_link();
            
    }


    @Override
	public int compareTo(DiagnosisBean o) {
		return this.getDisease_frequency() - o.getDisease_frequency();
	}
	
	@Override
	public boolean equals(Object o){
		if(o instanceof DiagnosisBean)
			return this.db_reference == ((DiagnosisBean) o).getDb_reference();
		else
			return false;
	}

    
	public String getDb_object_id() {
		return db_object_id;
	}


	public void setDb_object_id(String db_object_id) {
		this.db_object_id = db_object_id;
	}


	public database_name getDatabase() {
		return database;
	}


	public void setDatabase(database_name database) {
		this.database = database;
	}


	public String getHpo_id() {
		return hpo_id;
	}


	public void setHpo_id(String hpo_id) {
		this.hpo_id = hpo_id;
	}


	public String getDb_name() {
		return db_name;
	}


	public void setDb_name(String db_name) {
		this.db_name = db_name;
	}


	public qualifier_name getQualifier() {
		return qualifier;
	}


	public void setQualifier(qualifier_name qualifier) {
		this.qualifier = qualifier;
	}


	public String getDb_reference() {
		return db_reference;
	}


	public void setDb_reference(String db_reference) {
		this.db_reference = db_reference;
	}


	public String getEvidence_code() {
		return evidence_code;
	}


	public void setEvidence_code(String evidence_code) {
		this.evidence_code = evidence_code;
	}


	public int getDisease_frequency() {
		return disease_frequency;
	}


	public void setDisease_frequency(int disease_frequency) {
		this.disease_frequency = disease_frequency;
	}


	public String getWeb_link() {
		return web_link;
	}


	public void setWeb_link(String web_link) {
		this.web_link = web_link;
	}


	public Set<String> getOther_hpo_ids() {
		return other_hpo_ids;
	}


	public void setOther_hpo_ids(Set<String> other_hpo_ids) {
		this.other_hpo_ids = other_hpo_ids;
	}


	public Set<String> getLacking_hpos() {
		return lacking_hpos;
	}


	public void setLacking_hpos(Set<String> lacking_hpos) {
		this.lacking_hpos = lacking_hpos;
	}


	public Map<String, ClickValue> getEntry_clicks() {
		return entry_clicks;
	}


	public void setEntry_clicks(Map<String, ClickValue> entry_clicks) {
		this.entry_clicks = entry_clicks;
	}
	
	@Override
	public String toString() {
		String outstring = this.getDb_name();
		return outstring;
	}

}
