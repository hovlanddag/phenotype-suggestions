package no.uio.ifi.logid.rekvisisjon.hpo;

import java.io.IOException;
import java.util.List;
import java.util.Set;

import org.apache.jena.ontology.OntClass;
import org.apache.jena.ontology.OntModel;
import org.apache.jena.rdf.model.Resource;
import org.semanticweb.owlapi.model.IRI;
import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLOntologyStorageException;
import org.semanticweb.owlapi.reasoner.NodeSet;

public interface PhenotypeOntology {

		
		

		/**
		 * Assumes input is localname in ontology, e.g. HP_001622
		 * @param hpolocalname
		 * @return
		 * @throws HPOException 
		 */
		//public NodeSet<OWLClass> getSubclasses(String hpolocalname, boolean direct) throws HPOException;
				
		public List<Tickbox> getSubHPOEntries(String hpoparent, String hpocode, boolean direct) throws HPOException;
		
		//public List<Tickbox> getSubclasses(Resource cls, boolean direct) throws HPOException;

		
		//public OntModel getModel();

		
		public OWLClass getOntClass(String hpo_localname) ;
		public List<Tickbox> getTopClassTickboxes(boolean direct) ;

		public boolean hasSubclass(Resource hpoclass);

		public List<Tickbox> getPatientIDList();
	
	
		boolean hasSubclass(String hpo_localname) throws HPOException;

	
		boolean consistent(String patientid) throws HPOException;


		public NodeSet<OWLClass> getSubClasses(OWLClass phenocls, boolean b);

		public NodeSet<OWLClass> getSuperClasses(OWLClass phenocls, boolean b);

		List<Tickbox> codeToTickbox(List<String> cls_list, boolean navigate) throws HPOException;

		List<Tickbox> codeToTickbox(NodeSet<OWLClass> cls_list, String hpo_parent, boolean navigate) throws HPOException;

		List<Tickbox> iriToTickbox(List<IRI> cls_list, boolean navigate) throws HPOException;

		OWLClass getUMLSEntry(String umls_code);

		public boolean isSuperClass(OWLClass cls, OWLClass hpo_class);


}
