package no.uio.ifi.logid.rekvisisjon.hpo;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import no.uio.ifi.logid.rekvisisjon.CSVUtils;
import no.uio.ifi.logid.rekvisisjon.hpo.HPAnnotation.database_name;

/**
 * Reads and stores the configuration file for the top level categories shown in the requisition
 * @author dag
 *
 */
public class UMLS_MRCONSO_FileReader {


	private static final Logger LOG = LoggerFactory.getLogger(UMLS_MRCONSO_FileReader.class);

	private static UMLS_MRCONSO_FileReader instance = null;

	private Map<String, String> umls_to_omim_map;

	private UMLS_MRCONSO_FileReader(String config_file_path) throws IOException {

		Path list_path = FileSystems.getDefault().getPath(config_file_path, "MRCONSO.RRF");
		int line_no = 0;
		this.umls_to_omim_map = new HashMap<String, String>();
		LOG.debug("Reading file from: " + list_path.toString());
		BufferedReader reader = null;
		try {
			reader = Files.newBufferedReader(list_path);
			String line = null;
			while((line = reader.readLine()) != null){
				line_no ++;
				List<String> parsedline = CSVUtils.parseLine(line, '|');
				if(parsedline.isEmpty()){
					throw new IOException("Line " + line_no + " is empty in file " + list_path.toString());
				}
				if(parsedline.get(0) == null){
					LOG.error("Line " + line_no + " has empty required database name in file " + list_path.toString());
					continue;
				}
				String umls_cuid = parsedline.get(0);
				String db_local_id = parsedline.get(10);
				String dbname = parsedline.get(11);
				if(dbname.equals("OMIM")) {
					this.umls_to_omim_map.put(umls_cuid, db_local_id);
				}
				
			}
			if(line_no == 0)
				throw new IOException("The UMLS file \"" + list_path.toString() + "\" is empty");
			
		} catch (IOException e) {
			LOG.error("Error when reading from UMLS file at \"" + list_path.toString() + "\": " + e.getMessage());
			throw new IOException("Error when reading from configuration file " + list_path.toString() + " at line " + line_no + ": " + e.getMessage(), e);
		} finally {
			if(reader != null)
				reader.close();
		}
		assert(this.umls_to_omim_map != null);
	}
	
	public static synchronized UMLS_MRCONSO_FileReader getInstance(String configfolder) throws IOException {
		if(UMLS_MRCONSO_FileReader.instance == null)
			UMLS_MRCONSO_FileReader.instance = new UMLS_MRCONSO_FileReader(configfolder);
		assert(instance != null);
		return instance;
	}

	public static UMLS_MRCONSO_FileReader getInstance() {
		assert(instance != null);
		return instance;
	}

	public String getOMIMCode(String umls) {
		return "OMIM:" + this.umls_to_omim_map.get(umls);
	}
}
