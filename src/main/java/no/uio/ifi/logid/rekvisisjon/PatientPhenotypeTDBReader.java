package no.uio.ifi.logid.rekvisisjon;

import java.io.IOException;
import java.util.List;

import org.apache.jena.dboe.jenax.Txn;
import org.apache.jena.ontology.Individual;
import org.apache.jena.ontology.OntModel;
import org.apache.jena.ontology.OntModelSpec;
import org.apache.jena.query.Dataset;
import org.apache.jena.query.ReadWrite;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.riot.RDFDataMgr;
import org.apache.jena.tdb2.TDB2Factory;

import no.uio.ifi.logid.rekvisisjon.hpo.HPUtils;

public class PatientPhenotypeTDBReader implements PatientPhenotypeReader {
	private String config_file_path;
	private static PatientPhenotypeTDBReader instance = null;
	private Dataset dataset = null;

	private PatientPhenotypeTDBReader(String config_file_path){
		this.config_file_path = config_file_path;
		//String assemblerFile = config_file_path + "/tdb-assembler.ttl" ;
		String database_dir = config_file_path + "/rekvisisjon-database" ;
		dataset = TDB2Factory.createDataset(database_dir);
		Txn.executeWrite(dataset, ()->{
            RDFDataMgr.read(dataset, config_file_path + "/tdb-assembler.ttl");
       }) ;
	}

	public synchronized static PatientPhenotypeTDBReader getInstance(String config_file_path){
		if(instance == null)
			instance = new PatientPhenotypeTDBReader(config_file_path);
		return instance;
	}

	
	
	
	public PatientPhenotype read(String patient_id){
		dataset.begin(ReadWrite.READ);
		Model m = dataset.getNamedModel(HPUtils.getPatientURI(patient_id));
		OntModelSpec spec = new OntModelSpec( OntModelSpec.OWL_MEM_RDFS_INF );
		OntModel model = ModelFactory.createOntologyModel(spec, m);
		//model.read( "file:" + this.getPatientFile(patient_id) );
		Individual patient = HPUtils.getPatientIndividual(patient_id, model);
		dataset.end();
		//assert(patient != null);
		if(patient != null)
			return new PatientPhenotype(model, dataset, patient);
		else 
			return null;
	}

	public PatientPhenotype createNew(String patient_id) throws IOException{
		OntModel model = ModelFactory.createOntologyModel();
		HPUtils.addHPmodel(model, this.config_file_path);
		model =  HPUtils.createPatient(patient_id, model);
		Individual patient = HPUtils.getPatientIndividual(patient_id, model);
		
		dataset.begin(ReadWrite.WRITE);
		dataset.addNamedModel(HPUtils.getPatientURI(patient_id), model);
		dataset.end();
		
		assert(patient != null);
		return new PatientPhenotype(model, dataset, patient);
	}

	@Override
	public List<String> getPatientIDSList() throws IOException {
		// TODO Auto-generated method stub
		return null;
	}
}
