package no.uio.ifi.logid.rekvisisjon.hpo;

public class HPOException extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = -1804591816211048070L;

	public HPOException(String message){
		super(message);
	}
}
