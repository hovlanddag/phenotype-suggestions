package no.uio.ifi.logid.rekvisisjon.hpo;

import java.io.IOException;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import no.uio.ifi.logid.rekvisisjon.ResponseType;
import no.uio.ifi.logid.rekvisisjon.hpo.HPAnnotation.database_name;
import no.uio.ifi.logid.rekvisisjon.hpo.HPAnnotation.qualifier_name;

/**
 * Represents a single line in a HP annotations file
 * @author dag
 *
 */
public class HPAnnotation implements Comparable<HPAnnotation>, Serializable  {
	
	/**	
	 * 
	 */
	private static final long serialVersionUID = -7145542549414515163L;
	public enum qualifier_name {
		HPO_CODE, NOT, SECONDARY, MILD,MODERATE,SEVERE,PROGRESSIVE, PROXIMAL,CHRONIC,  NO_QUALIFIER, DISTAL, BILATERAL, EPISODIC, PROFOUND, REFRACTORY, RECURRENT, GENERALIZED, NONPROGRESSIVE, ORPHA, UNILATERAL;
	}
	// Only non-null if qualifier_name is HPO_CODE
	public String qualifier_hpo_code; // Optional qualifier specificed by HPO code. 
	
	public enum database_name {
		OMIM, DECIPHER, ORPHA
	}
	
	
	
	
	private database_name database;
	private String db_object_id; // Id in the database, e.g. 154700
	private String hpo_id; // Id in HP. E.g. HP:0002345
	private String db_name; // Name given in database
	private qualifier_name qualifier; // optional E.g. not
	private String db_reference; // OMIM:154700 or PMID:15517394
	private String evidence_code; // optional E.g. IEA
	private int disease_frequency; // number of entries in which this disease occurs
	private String web_link; // Adress of web page about disease
	private Set<String> other_hpo_ids; // The other HPO ids annotated with this disease
	
	public static Map<ResponseType, String> enumNames;
	static {
		enumNames = new HashMap<ResponseType, String>();
		enumNames.put(ResponseType.NOT_PRESENT, "Examined patient, not present");
		enumNames.put(ResponseType.TOO_YOUNG, "Age of onset not reached");
		enumNames.put(ResponseType.CANNOT_CHECK, "Unable to examine patient");
		enumNames.put(ResponseType.CANNOT_PRESENT, "Manifestation unrealizable");
	}
	
	
	public HPAnnotation(database_name database, String db_object_id, String web_link, String hpo_id, String db_name, String db_reference,
			String evidence_code) {
		super();
		this.database = database;
		this.db_object_id = db_object_id;
		this.web_link = web_link;
		this.hpo_id = hpo_id;
		this.db_name = db_name;
		this.db_reference = db_reference;
		this.evidence_code = evidence_code;
		this.disease_frequency = 0;
	}
	public HPAnnotation(database_name database, String db_local_id, String web_link, String db_local_name, qualifier_name qualifier,
			String hpo_localname,  String db_reference, String evidence_code) {
		this.database = database;
		this.db_name = db_local_name;
		this.db_object_id = db_local_id;
		this.web_link = web_link;
		this.qualifier = qualifier;
		this.hpo_id = hpo_localname;
		this.db_reference = db_reference;
		this.evidence_code = evidence_code;
		this.disease_frequency = 0;
	}
	public database_name getDatabase() {
		return database;
	}
	public void setDatabase(database_name database) {
		this.database = database;
	}
	/**
	 * 
	 * @return Return the local ID of the disease or gene in the related ontology (E.g. the number id in OMIM)
	 */
	public String getDb_object_id() {
		return db_object_id;
	}
	public void setDb_object_id(String db_object_id) {
		this.db_object_id = db_object_id;
	}
	
	/**
	 * @return Web adress of disease info page
	 */
	public String getWeb_link(){
		return this.web_link;
	}
	public void setWeb_link(String link){
		this.web_link = link;
	}
	
	/**
	 * @return The ID of the related hpo entry, as given in the HP ontology
	 */
	public String getHpo_id() {
		return hpo_id;
	}
	public void setHpo_id(String hpo_id) {
		this.hpo_id = hpo_id;
	}
	/**
	 * 
	 * @return The name of the disease or gene in the related external ontology
	 */
	public String getDb_name() {
		return db_name;
	}
	public void setDb_name(String db_name) {
		this.db_name = db_name;
	}
	public qualifier_name getQualifier() {
		return qualifier;
	}
	public void setQualifier(qualifier_name qualifier) {
		this.qualifier = qualifier;
	}
	public String getDb_reference() {
		return db_reference;
	}
	public void setDb_reference(String db_reference) {
		this.db_reference = db_reference;
	}
	public String getEvidence_code() {
		return evidence_code;
	}
	public void setEvidence_code(String evidence_code) {
		this.evidence_code = evidence_code;
	}
	
	
	public int getDisease_frequency() {
		return disease_frequency;
	}
	public void setDisease_frequency(int disease_frequency) {
		this.disease_frequency = disease_frequency;
	}
	public static qualifier_name parseQualifier(String qualifier) throws IOException{
		if(qualifier == null || qualifier.equals(""))
			return qualifier_name.NO_QUALIFIER;
		if(qualifier.equals("NOT"))
			return qualifier_name.NOT;
		else if(qualifier.equals("SECONDARY"))
			return qualifier_name.SECONDARY;
		else if(qualifier.equals("MILD"))
			return qualifier_name.MILD;
		else if(qualifier.equals("MODERATE"))
			return qualifier_name.MODERATE;
		else if(qualifier.equals("SEVERE"))
			return qualifier_name.SEVERE;
		else if(qualifier.equals("PROFOUND"))
			return qualifier_name.PROFOUND;
		
		else if(qualifier.equals("PROGRESSIVE"))
			return qualifier_name.PROGRESSIVE;
		else if(qualifier.equals("NONPROGRESSIVE"))
			return qualifier_name.NONPROGRESSIVE;
		else if(qualifier.equals("CHRONIC"))
			return qualifier_name.CHRONIC;
		else if(qualifier.equals("EPISODIC"))
			return qualifier_name.EPISODIC;
		else if(qualifier.equals("RECURRENT"))
			return qualifier_name.RECURRENT;
		
		else if(qualifier.equals("REFRACTORY"))
			return qualifier_name.REFRACTORY;
		else if(qualifier.equals("GENERALIZED"))
			return qualifier_name.GENERALIZED;
		
		else if(qualifier.equals("PROXIMAL"))
			return qualifier_name.PROXIMAL;
		else if(qualifier.equals("DISTAL"))
			return qualifier_name.DISTAL;
		else if(qualifier.equals("BILATERAL"))
			return qualifier_name.BILATERAL;
		else if(qualifier.equals("HP:0012833"))
			return qualifier_name.UNILATERAL;
		else if(qualifier.equals("HP:0012825"))
			return qualifier_name.MILD;
		else if(qualifier.substring(0, 3).equals("HP:"))
			return qualifier_name.HPO_CODE;
		else 
			throw new IOException("HPAnnotation.java: Not a valid qualifier: \"" + qualifier + "\"");	
	}
	
	public static database_name parseDatabaseName(String database) throws IOException{
		assert(database != null);
		if(database.equals("DECIPHER"))
			return database_name.DECIPHER;
		else if(database.equals("OMIM"))
			return database_name.OMIM;
		else if(database.equals("ORPHA"))
			return database_name.ORPHA;
		else 
			throw new IOException("HPAnnotation.java: Not a valid database name: \"" + database + "\"");	
	}
	@Override
	public int compareTo(HPAnnotation o) {
		return this.getDisease_frequency() - o.getDisease_frequency();
	}
	
	@Override
	public boolean equals(Object o){
		if(o instanceof HPAnnotation)
			return this.db_reference == ((HPAnnotation) o).getDb_reference();
		else
			return false;
	}
	public Set<String> getOther_hpo_ids() {
		return other_hpo_ids;
	}
	public void setOther_hpo_ids(Set<String> other_hpo_ids) {
		this.other_hpo_ids = other_hpo_ids;
	}
	
		@Override
		public String toString() {
			return this.getHpo_id();
		}
	
}
