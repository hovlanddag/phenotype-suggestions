package no.uio.ifi.logid.rekvisisjon;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.jena.rdf.model.Resource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import no.uio.ifi.logid.rekvisisjon.hpo.HPOException;
import no.uio.ifi.logid.rekvisisjon.hpo.OntologyFactory;
import no.uio.ifi.logid.rekvisisjon.hpo.PhenotypeList;
import no.uio.ifi.logid.rekvisisjon.hpo.PhenotypeOntology;
import no.uio.ifi.logid.rekvisisjon.hpo.Tickbox;

/**
 * Reads and stores the configuration file for the top level categories shown in the requisition
 * @author dag
 *
 */
public class PhenotypeListReader {


	private PhenotypeList list;

	private static final Logger LOG = LoggerFactory.getLogger(PhenotypeListReader.class);

	private static PhenotypeListReader instance = null;


	private PhenotypeListReader(String config_file_path) throws IOException {

		Path list_path = FileSystems.getDefault().getPath(config_file_path, "rekvisisjon.csv");

		LOG.debug("Reading file from: config_file_path");
		BufferedReader reader = null;
		try {
			reader = Files.newBufferedReader(list_path);
			Map<String, List<Tickbox>> tickboxes = new HashMap<String, List<Tickbox>>();
			String line = reader.readLine(); // First line is header, omit
			if(line == null)
				throw new IOException("The rekvisisjon file \"" + list_path.toString() + "\" is empty");
			List<Tickbox> current_cat_list = new ArrayList<Tickbox>();
			line = reader.readLine();
			String current_cat_name = line.split(",")[0];
			assert(current_cat_name.length() > 1);
			int line_no = 2;
			PhenotypeOntology hpowl = OntologyFactory.getOntology();
			List<String> hpolocalnames = new ArrayList<String>();
			while((line = reader.readLine()) != null){
				line_no ++;
				List<String> parsedline = CSVUtils.parseLine(line);
				if(parsedline.isEmpty()){
					throw new IOException("Line " + line_no + " is empty in file " + list_path.toString());
				}

				// New header section. Only first column is non-empty
				if(parsedline.get(0) != null && (parsedline.size() == 1 || parsedline.get(1) == null || parsedline.get(1).length() == 0 )){
					if (current_cat_list.size() > 0){
						tickboxes.put(current_cat_name, current_cat_list);
					}
					current_cat_name = parsedline.get(0);
					current_cat_list = new ArrayList<Tickbox>();

					continue;
				}
				if(parsedline.get(1) == null){
					LOG.error("Line " + line_no + " has empty second column in file " + list_path.toString());
					continue;
				}
				if(parsedline.get(0) == null){
					LOG.error("Line " + line_no + " has empty first column in file " + list_path.toString());
					continue;
				}
				String hpo_localname = parsedline.get(1);
				assert(hpo_localname != null);
				//if(!hpo_localname.substring(0,3).leng)
				//	continue;
				if(! hpo_localname.substring(0,3).equals("HP_")){
					try {
						int cif = Integer.parseInt(hpo_localname);
					} catch (NumberFormatException nfe) {
						LOG.error("Line " + line_no + " has invalid HPO code " + hpo_localname);
						continue;
					}
					while(hpo_localname.length() <  7)
						hpo_localname = "0" + hpo_localname;
					hpo_localname = "HP_" + hpo_localname;
				}
				hpolocalnames.add(hpo_localname);
				String label = parsedline.get(0);
				assert(label != null);
				//Resource hpoclass = null;
				//boolean hasSubclasses = true;
				
				boolean hasSubclasses = false;
				try {
					//hpoclass = hpowl.getOntClass(hpo_localname);
					hasSubclasses = hpowl.hasSubclass(hpo_localname);
				}catch(HPOException e) {
					LOG.debug("Line " + line_no + " has wrong HPO class name in second column in file " + list_path.toString());
				}
				
				Tickbox tb;
				tb = new Tickbox(label,current_cat_name,hpo_localname,parsedline.get(6), hasSubclasses);
				current_cat_list.add(tb);
			}
			if(current_cat_list.size() > 0){
				tickboxes.put(current_cat_name, current_cat_list);
			}
			this.list = new PhenotypeList(tickboxes);
		} catch (IOException e) {
			LOG.error("Error when reading from TopLevelList file at \"" + list_path.toString() + "\": " + e.getMessage());
			throw new IOException("Error when reading from configuration file", e);
		} finally {
			if(reader != null)
				reader.close();
		}
		assert(this.list != null);
	}

	public static synchronized PhenotypeListReader getInstance(String configfolder) throws IOException {
		if(PhenotypeListReader.instance == null)
			PhenotypeListReader.instance = new PhenotypeListReader(configfolder);
		return instance;
	}

	public PhenotypeList getTickboxes(){
		return list;
	}

	public static PhenotypeListReader getInstance() {
		assert(instance != null);
		return instance;
	}
}
