package no.uio.ifi.logid.rekvisisjon.hpo;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import org.semanticweb.owlapi.model.IRI;
import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLOntologyStorageException;
import org.semanticweb.owlapi.reasoner.NodeSet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import no.uio.ifi.logid.rekvisisjon.ResponseType;


public class PhenotypeState implements Serializable {

	
	/**
	 * This object is in the HTTPSession. Maintains state of interface, assertions etc.
	 */
	private static Logger LOG = LoggerFactory.getLogger(PhenotypeState.class);
	
	
	private Set<IRI> assertions;  // "YES" entries
	private Set<IRI> negations;	// "NO" entries
	private Map<IRI, ResponseType> reasons; // Reason for "NO" entries
	
	private PhenotypeOntology reasoner;
	
	private Set<Diagnosis> diags;
	private Boolean diags_stale; // True if diags must be rebuilt, e.g. new phenotype
	private Lock diags_locks; // Preventing simultaneous access to diags
	
	
	/**
	 * Loads hp.owl and keeps in-memory materialized in a local RDFOx database. 
	 * 
	 * @param config_path
	 * @throws IOException 
	 */
	public PhenotypeState()  {
		init();
			try {
				reasoner = OntologyFactory.getOntology();
			} catch (IOException e) {
				LOG.error("Error when loading phenotype ontology: " + e);
				e.printStackTrace();
			}
	}
	
	private void init()  {
		this.assertions = new HashSet<IRI>();
		this.negations = new HashSet<IRI>();
		this.diags = new HashSet<Diagnosis>();
		this.diags_stale = true;
		this.diags_locks = new ReentrantLock();
			try {
				reasoner = OntologyFactory.getOntology();
			} catch (IOException e) {
				LOG.error("Error when loading phenotype ontology: " + e);
				e.printStackTrace();
			}
	}

	
	public void reset(){
		for(IRI a : this.assertions){
			this.assertions.remove(a);
		}
		for(IRI a : this.negations){
			this.negations.remove(a);
		}
		
	}
	
	
	
	
	/**
	 * Returns true iff there is an axiom stating explicitly pat is of class phenocls, or of a subclass.
	 * So, expanding subclass of axioms  
	 * So if true, stating that patient is not phenocls is inconsistent
	 * @param pat
	 * @param phenocls
	 * @return
	 * @throws HPOException
	 * @throws IOException 
	 * @throws OWLOntologyStorageException 
	 */
	public boolean patientHasPhenotype(String hpo_code) throws HPOException, OWLOntologyStorageException, IOException {
		OWLClass phenocls = (OWLClass) reasoner.getOntClass(hpo_code);
		return patientHasPhenotype(phenocls);
	}
	

	/**
	 * Returns true iff the patient_id is not of class hpo_code
	 * That is, hpo_code, or a superclass is explicitly not a type of patient_id
	 * Since negation propagates upwards, this means that adding a positive statements about phenocls is inconsistent
	 * @param patient
	 * @param phenocls
	 * @return
	 * @throws IOException 
	 * @throws OWLOntologyStorageException 
	 */
	public boolean patientHasNegPhenotype(String hpo_code) throws HPOException, OWLOntologyStorageException, IOException{
		OWLClass phenocls = reasoner.getOntClass(hpo_code);
		return patientHasNegPhenotype(phenocls);
	}

	
	/**
	 * Returns true iff there is an axiom stating pat is of class phenocls, or of a subclass. 
	 * @param pat
	 * @param phenocls
	 * @return
	 * @throws HPOException
	 * @throws IOException 
	 * @throws OWLOntologyStorageException 
	 */
	private boolean patientHasPhenotype(OWLClass phenocls) throws HPOException, OWLOntologyStorageException, IOException{
		diags_locks.lock();
		NodeSet<OWLClass> subnodes = reasoner.getSubClasses(phenocls, false);
		diags_locks.unlock();
		return overlap(assertions, subnodes, phenocls);
	}

	/**
	 * Returns true iff the patient is stated to not be of a class, (or its superclass)
	 * @param patient
	 * @param phenocls
	 * @return
	 * @throws IOException 
	 * @throws OWLOntologyStorageException 
	 */
	private boolean patientHasNegPhenotype(OWLClass phenocls) throws OWLOntologyStorageException, IOException{
		diags_locks.lock();
		NodeSet<OWLClass> supernodes = reasoner.getSuperClasses(phenocls, false);
		diags_locks.unlock();
		return overlap(negations, supernodes, phenocls);
	}
	
	
	/**
	 * returns true if nodes+phenocls overlaps with phenos
	 * @param phenos
	 * @param nodes
	 * @param phenocls
	 * @return
	 */
	private boolean overlap(Set<IRI> phenos, NodeSet<OWLClass> nodes, OWLClass phenocls){
		Set<OWLClass> supers = nodes.getFlattened();
		supers.add(phenocls.asOWLClass());
		for(OWLClass sup : supers){
			if(phenos.contains(sup.getIRI()))
				return true;		
		}
		return false;
	}	

	public Set<IRI> getPatientPhenotypes() throws HPOException{
		return assertions;
	}
	
	public Set<IRI> getPatientNegPhenotypes() throws HPOException{
		return negations;
	}
	
	public boolean setPositivePatientPhenotype(String hpo_code) throws OWLOntologyStorageException, HPOException, IOException {
		OWLClass phenocls = reasoner.getOntClass(hpo_code);
		return this.setPositivePatientPhenotype(phenocls);
	}

	public boolean setPositivePatientPhenotype(OWLClass phenocls) throws OWLOntologyStorageException, HPOException, IOException {
		IRI phenoiri = phenocls.getIRI();
		if(patientHasNegPhenotype(phenocls))
			return false;
		diags_locks.lock();
		assertions.add(phenoiri);
		diags_stale = true;
		diags_locks.unlock();
		return true;
	}

	
	public boolean setNegativePatientPhenotype(String hpo_code) throws OWLOntologyStorageException, HPOException, IOException {
		OWLClass phenocls = reasoner.getOntClass(hpo_code);
		return this.setNegativePatientPhenotype(phenocls);
	}

	public boolean setNegativePatientPhenotype(OWLClass phenocls) throws OWLOntologyStorageException, HPOException, IOException {
		IRI phenoiri = phenocls.getIRI();
		if(patientHasPhenotype(phenocls))
			return false;
		diags_locks.lock();
		negations.add(phenoiri);
		diags_stale = true;
		diags_locks.unlock();
	    return true;
	}

	
	public void setNegativePhenotypeReason(String hpo_code, ResponseType reason) throws HPOException{
		String phenouri = HPUtils.getHPuri(hpo_code);
		IRI phenoiri = IRI.create(phenouri);
		reasons.put(phenoiri, reason);
	}
	
	
	public ResponseType getNegativePhenotypeReason(String hpo_code) throws HPOException{
		String phenouri = HPUtils.getHPuri(hpo_code);
		IRI phenoiri = IRI.create(phenouri);
		return reasons.get(phenoiri);
	}



	public void removePatientNegPhenotypeReason(String hpo_code) throws HPOException {
		String phenouri = HPUtils.getHPuri(hpo_code);
		IRI phenoiri = IRI.create(phenouri);
		reasons.remove(phenoiri);
	}
	
	
	public boolean removePatientNegPhenotype(String hpo_code)
			throws OWLOntologyStorageException, HPOException, IOException {
		String phenouri = HPUtils.getHPuri(hpo_code);
		IRI phenoiri = IRI.create(phenouri);
		diags_stale= true;
		return negations.remove(phenoiri);
	}
	
	public boolean removePatientNegPhenotype(OWLClass hpo_class)
			throws OWLOntologyStorageException, HPOException, IOException {
		IRI phenoiri = hpo_class.getIRI();
		diags_stale= true;
		return negations.remove(phenoiri);
	}

	public boolean removePatientPositivePhenotype(String hpo_code)
			throws OWLOntologyStorageException, HPOException, IOException {
		String phenouri = HPUtils.getHPuri(hpo_code);
		IRI phenoiri = IRI.create(phenouri);
		diags_stale= true;
		return assertions.remove(phenoiri);
	}

	public boolean removePatientPositivePhenotype(OWLClass hpo_class)
			throws OWLOntologyStorageException, HPOException, IOException {
		IRI phenoiri = hpo_class.getIRI();
		diags_stale= true;
		return assertions.remove(phenoiri);
	}

	
	public List<Tickbox> getSymptomSuggestions() throws HPOException, IOException {
		updateDiags();

		Set<String> sympt_beans = new HashSet<String>();
		for(Diagnosis d : diags){
			if(d.getRho() < 0.2)
				for(String symp : d.getLackingHpos()) {
					assert(symp.substring(0,3).equals("HP_"));
					sympt_beans.add(symp);
					try {
						OWLClass hpocls = reasoner.getOntClass(symp);
						for(OWLClass sup : reasoner.getSuperClasses(hpocls, false).getFlattened()){
							String sup_code = sup.getIRI().getFragment();
							if(sup_code.substring(0,2).equals("HP"))
								sympt_beans.add(sup_code);
						}
					} catch(IllegalArgumentException e) {
						LOG.error("Error with hpo code " + symp + 
								" from diagnosis " + d.getAnnotation().getDb_name() + 
								" Error: " + e.toString());
					}
				}
		}
		
		for(IRI hpoiri : assertions){
			sympt_beans.remove(hpoiri.getFragment());
		}
		for(IRI hpoiri : negations){
			sympt_beans.remove(hpoiri.getFragment());
		}
		
		PhenotypeOntology hp = OntologyFactory.getOntology();
		
		List<Tickbox> sympt_list = hp.codeToTickbox(new ArrayList<String>(sympt_beans), false);
		
		return sympt_list;
	}
	
	private void updateDiags(){
		diags_locks.lock();
		if(diags_stale){
			HPAnnotationsFileReader fr = HPAnnotationsFileReader.getInstance();
			Set<HPAnnotation> annots = new HashSet<HPAnnotation>();
			for(IRI hpoiri : assertions){
				Set<HPAnnotation> clsannots = fr.getAnnotations(hpoiri.getFragment());
				if(clsannots != null)
					annots.addAll(clsannots);
				OWLClass hpocls;
				try {
					hpocls = reasoner.getOntClass(hpoiri.getFragment());
					for(OWLClass sup : reasoner.getSuperClasses(hpocls, false).getFlattened()){
						String sup_code = sup.getIRI().getFragment();
						Set<HPAnnotation> supannots = fr.getAnnotations(sup_code);
						if(supannots != null)
							annots.addAll(supannots);
					}
				} catch (IllegalArgumentException e) {
					LOG.error("Error handling IRI " + hpoiri);
				}
			}
			for(IRI hpoiri : negations){
				Set<HPAnnotation> clsannots = fr.getAnnotations(hpoiri.getFragment());
				if(clsannots != null)
					annots.removeAll(clsannots);
				OWLClass hpocls;
				try {
					hpocls = reasoner.getOntClass(hpoiri.getFragment());
					for(OWLClass sub : reasoner.getSubClasses(hpocls, false).getFlattened()){
						String sub_code = sub.getIRI().getFragment();
						Set<HPAnnotation> subannots = fr.getAnnotations(sub_code);
						if(subannots != null)
							annots.removeAll(subannots);
					}
				} catch (IllegalArgumentException e) {
					LOG.error("Error handling IRI " + hpoiri);
					e.printStackTrace();
				}	
			}
			
			Set<Diagnosis> new_diags = new HashSet<Diagnosis>();
			for(HPAnnotation annot : annots){
				Diagnosis d = new Diagnosis(annot);
				for(IRI hpoiri : assertions){
					d.addHPO(hpoiri.getFragment());
					if(d.getLackingHpos().contains(hpoiri.getFragment()))
							LOG.error(hpoiri + " not removed from Diagnosis " + d);
				}
				new_diags.add(d);
			}
			diags = new_diags;
			diags_stale= false;
		}
		diags_locks.unlock();
	}
	
	public synchronized Set<DiagnosisBean> getAnnotations() {
		updateDiags();
		Set<DiagnosisBean> diag_beans = new HashSet<DiagnosisBean>();
		for(Diagnosis d : diags){
			DiagnosisBean b = new DiagnosisBean(d);
			diag_beans.add(b);
		}
		return diag_beans;
	}




}

