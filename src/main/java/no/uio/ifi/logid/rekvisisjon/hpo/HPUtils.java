package no.uio.ifi.logid.rekvisisjon.hpo;

import java.io.IOException;

import org.apache.jena.atlas.logging.Log;
import org.apache.jena.ontology.Individual;
import org.apache.jena.ontology.OntClass;
import org.apache.jena.ontology.OntDocumentManager;
import org.apache.jena.ontology.OntModel;
import org.apache.jena.query.Dataset;
import org.apache.jena.query.ReadWrite;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Utility operations related to HP ontology
 * 
 */
public class HPUtils {

	private static final Logger LOG = LoggerFactory.getLogger(HPUtils.class);

	public static String HP_ONTO_URI = "http://purl.obolibrary.org/obo/hp.owl";
	public static String FORM_STORE_URI = "http://ifi.uio.no/logid/phenotype/form";
	public static final String FORM_CLASS_URI = "http://ifi.uio.no/logid/phenotype/form";
	
			public static String hasPhenotypeURI = "http://ifi.uio.no/logid/hasPhenotype";
	
	/**
	 * 
	 * @param hpo_localname Of the form: HP:000162 or HP_000162
	 * @return proper uri in the ontology, e.g. http://..../HP_0000162 
	 * @throws HPOException 
	 */
	public static String getHPuri(String hpo_localname) throws IllegalArgumentException{
		hpo_localname = hpo_localname.replace("\"", "");
		if(hpo_localname.substring(0,3).equals("HP_")){
			return "http://purl.obolibrary.org/obo/" + hpo_localname;
		} else if (hpo_localname.substring(0,3).equals("HP:")) {
			return "http://purl.obolibrary.org/obo/HP_" + hpo_localname.substring(3);
		} else {
			LOG.error("Unexpected hpo localname: " + hpo_localname);
			throw new IllegalArgumentException("Unexpected hpo localname: " + hpo_localname);
		}
	}

	
	/**
	 * Take hpo code or localname and returns information page in ontobee
	 * @param hpo_localname_or_code
	 * @return
	 * @throws HPOException 
	 */
	public static String getInfoPageURL(String hpo_localname_or_code) throws HPOException{
		return "http://www.ontobee.org/ontology/HP?iri=" + getHPuri(hpo_localname_or_code);
	}
	/**
	 * 
	 * @param hpo_localname, usually on the form HP_0000162
	 * @param m Ontology that includes the HPO ontology
	 * @return class in the ontology corresponding to hpo_code
	 * @throws HPOException 
	 */
	public static OntClass getOntClass(String hpo_localname, OntModel m) throws HPOException{
		assert(hpo_localname != null);
		String classuri = HPUtils.getHPuri(hpo_localname);
		
		OntClass cl = m.getOntClass(classuri);
		
		if(cl == null){
			throw new HPOException("There is no class with URI " + classuri + " (generated from id " + hpo_localname + ") in the ontology ");
		}
		
		return cl;
	}

	
	
	public static OntModel addHPreference(OntModel model, String config_file_path){
		OntDocumentManager dm = model.getDocumentManager();
		dm.addAltEntry( "http://purl.obolibrary.org/obo/hp.owl",
				"file:" + config_file_path + "/hp.owl" );
		return model;
	}

	public static OntModel addHPmodel(OntModel model, String config_file_path) throws IOException{
		addHPreference(model, config_file_path);
		PhenotypeOntologyFile HPowl = PhenotypeOntologyFile.getInstance(config_file_path);
		OntModel hpo = HPowl.getModel();
		model.addSubModel(hpo);
		//model.addLoadedImport("http://purl.obolibrary.org/obo/hp.owl");
		return model;
	}

	public static String getPatientURI(String patient_id){
		return "http://ifi.uio.no/logid/rekvisisjon/data/patient/" + patient_id;
	}

	public static OntModel createPatient(String patient_id, OntModel m){
		OntClass patient_class = m.createClass(FORM_CLASS_URI);

		m.addLoadedImport("http://purl.obolibrary.org/obo/hp.owl");

		assert(patient_class != null);
		Individual i = m.createIndividual(getPatientURI(patient_id), patient_class);
		assert(i != null);
		return m;
	}


	public static Individual getPatientIndividual(String patient_id, OntModel m){
		String patienturi = getPatientURI(patient_id);
		Individual i = m.getIndividual(patienturi);
		//assert(i != null);
		return i;
	}
}
