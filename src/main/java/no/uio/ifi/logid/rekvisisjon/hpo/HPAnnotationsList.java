package no.uio.ifi.logid.rekvisisjon.hpo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.Property;

import no.uio.ifi.logid.rekvisisjon.PatientPhenotype;

public class HPAnnotationsList implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4175221010546454264L;
	private Map<String, Set<HPAnnotation>> hp_to_annotations_map;
	private Map<String, Integer> disease_frequency;
	private HashMap<String, Set<String>> disease_to_hp_map;
	
	public HPAnnotationsList(){
		this.hp_to_annotations_map = new HashMap<String, Set<HPAnnotation>>();
		this.disease_to_hp_map = new HashMap<String, Set<String>>();
		this.disease_frequency = new HashMap<String, Integer>();
	}
	
	public void addAnnotation(String hpo_id, HPAnnotation annot){
		assert(hpo_id.substring(0, 3).equals("HP_"));
		if(hpo_id.substring(2, 3).equals(":"))
			hpo_id = hpo_id.replace(':', '_');
		Set<HPAnnotation> cur_hpid_annots = (this.hp_to_annotations_map.containsKey(hpo_id)) 
				? this.hp_to_annotations_map.get(hpo_id) 
						: new HashSet<HPAnnotation>();
		cur_hpid_annots.add(annot);
		if(!this.disease_to_hp_map.containsKey(annot.getDb_reference()))
			this.disease_to_hp_map.put(annot.getDb_reference(), new HashSet<String>());
		this.disease_to_hp_map.get(annot.getDb_reference()).add(annot.getHpo_id());
		this.hp_to_annotations_map.put(hpo_id, cur_hpid_annots);
	}
	
	public Set<HPAnnotation> getAnnotations(String hpo_id){
		return this.hp_to_annotations_map.get(hpo_id);
	}
	
	/**
	 * Returns all hpo codes linked with the disease code
	 * @param disease_id
	 * @return
	 */
	public Set<String> getDiseaseAnnotations(String disease_id){
		if(!this.disease_to_hp_map.containsKey(disease_id))
			this.disease_to_hp_map.put(disease_id, new HashSet<String>());
		return this.disease_to_hp_map.get(disease_id);
	}
		
	
	public synchronized void increaseDiseaseFrequency(String disease){
		if(!this.disease_frequency.containsKey(disease)){
			this.disease_frequency.put(disease, 1);
		} else {
			Integer old_value = this.disease_frequency.get(disease);
			this.disease_frequency.put(disease, old_value + 1);
		}
	}
	
	public Integer getDiseaseFrequence(String disease){
		assert(this.disease_frequency.containsKey(disease));
		return this.disease_frequency.get(disease);
	}
	
	/**
	 * Updates disease frequencies in all annotations
	 */
	public void setAnnotDiseaseFrequencies(){
		for(Set<HPAnnotation> annots : this.hp_to_annotations_map.values()){
			for(HPAnnotation annot : annots){
				annot.setDisease_frequency(this.disease_frequency.get(annot.getDb_reference()));
				annot.setOther_hpo_ids(this.disease_to_hp_map.get(annot.getDb_reference()));
			}
		}
		
	}
	
	
	
}
