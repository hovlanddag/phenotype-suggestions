package no.uio.ifi.logid.rekvisisjon;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.nio.file.FileSystems;
import java.nio.file.Path;

public class ConfigFiles {
	private static String config_file_path;
	private static String HP_TTL_FILENAME = "hp.ttl";
	private static String HP_OWL_FILENAME = "hp.owl";
	private static ConfigFiles instance;
	
	
	private ConfigFiles(String _config_file_path){
		config_file_path = _config_file_path;
	}
	
	public static ConfigFiles getInstance(String config_file_path){
		if(instance == null)
			instance = new ConfigFiles(config_file_path);
		return instance;
	}
	
	public static ConfigFiles getInstance(){
		assert(instance != null);
		return instance;
	}
	
	/**
	 * Returns true if hp.owl is in the config_file_path
	 * @return
	 */
	public boolean hasHPOwl(){
		return hasFile(HP_OWL_FILENAME);
	}
	
	/**
	 * Returns true if hp.ttl is in the config_file_path
	 * @return
	 */
	public boolean hasHPTtl(){
		return hasFile(HP_TTL_FILENAME);
	}
	
	
	/**
	 * Returns true if hp.ttl is in the config_file_path
	 * @return
	 */
	public boolean hasTopLevelList(){
		return hasFile("rekvisisjon.csv");
	}
	
	
	/**
	 * Returns true if filename is in the config_file_path
	 * @return
	 */
	public boolean hasFile(String filename){
		Path hp_path = FileSystems.getDefault().getPath(config_file_path, filename);
		File hp_f = hp_path.toFile();
		return hp_f.exists();
	}

	public File getHPTtlFile() {
		return getFile(HP_TTL_FILENAME);
	}


	public File getHPOwlFile() {
		return getFile(HP_OWL_FILENAME);
		
	}
	

	public File getTopLevelFile() {
		return getFile("rekvisisjon.csv");
		
	}
	

	public void downloadHPOwl() throws IOException {
		URL website = new URL("http://purl.obolibrary.org/obo/hp.owl");
		ReadableByteChannel rbc = Channels.newChannel(website.openStream());
		File hpowl = getHPOwlFile();
		
		FileOutputStream fos = new FileOutputStream(hpowl);
		fos.getChannel().transferFrom(rbc, 0, Long.MAX_VALUE);
		fos.close();
	}
	

	public File getFile(String name) {
		Path hp_path = FileSystems.getDefault().getPath(config_file_path, name);
		return hp_path.toFile();
		
	}



	
}
