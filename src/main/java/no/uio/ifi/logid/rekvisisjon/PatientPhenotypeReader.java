package no.uio.ifi.logid.rekvisisjon;

import java.io.IOException;
import java.util.List;

public interface PatientPhenotypeReader {
	
	
	
	
	public PatientPhenotype read(String patient_id);
	
	public PatientPhenotype createNew(String patient_id) throws IOException;
	
	public List<String> getPatientIDSList() throws IOException;
}
