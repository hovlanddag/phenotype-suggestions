package no.uio.ifi.logid.rekvisisjon;

import static org.junit.Assert.assertTrue;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.junit.Test;
import org.semanticweb.owlapi.model.IRI;
import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLOntologyStorageException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import no.uio.ifi.logid.rekvisisjon.hpo.OntologyFactory;
import no.uio.ifi.logid.rekvisisjon.hpo.PhenotypeOntology;
import no.uio.ifi.logid.rekvisisjon.hpo.DiagnosisBean;
import no.uio.ifi.logid.rekvisisjon.hpo.HPAnnotationsFileReader;
import no.uio.ifi.logid.rekvisisjon.hpo.HPOException;
import no.uio.ifi.logid.rekvisisjon.hpo.HPUtils;
import no.uio.ifi.logid.rekvisisjon.hpo.PhenotypeOntologySequoia;
import no.uio.ifi.logid.rekvisisjon.hpo.PhenotypeState;
import no.uio.ifi.logid.rekvisisjon.hpo.PhenotypeTree;
import no.uio.ifi.logid.rekvisisjon.hpo.Tickbox;
import no.uio.ifi.logid.rekvisisjon.hpo.UMLS_MRCONSO_FileReader;

public class CommandLine implements Serializable {
	private static final long serialVersionUID = -8745209942211084833L;
	private static final Logger LOG = LoggerFactory.getLogger(CommandLine.class);
	private static final String readyString = "Ready for command \"setPhenotype <class> <value>\", \"getSuggestions\", \"getDiseases\", \"listPhenotypes\", \"testSuggestions\" <OMIM code> <MMI filename> or \"reset\"\n" +
			"<class>= HP:01234|UMLS:C1234";


	private String config_file_path;
	private PhenotypeState ps;
	private PhenotypeOntology ontology;

	public CommandLine(String config_file_path)  {
		this.config_file_path = config_file_path;
	}

	public synchronized void init() throws IOException, InterruptedException {
		ConfigFiles cfg = ConfigFiles.getInstance(config_file_path);
		if(!cfg.hasHPOwl())
			cfg.downloadHPOwl();
		OntologyFactory.init(config_file_path, OntologyFactory.backend_name.Sequoia);
		this.ontology = OntologyFactory.getOntology();
		//PhenotypeOntologyFile.getInstance(config_file_path);
		PhenotypeListReader.getInstance(config_file_path);
		PatientPhenotypeFileReader.getInstance(config_file_path);
		HPAnnotationsFileReader.getInstance(config_file_path);
		UMLS_MRCONSO_FileReader.getInstance(config_file_path);
		this.ps = new PhenotypeState();
	}

	public void reset() {
		ps.reset();
		ps = new PhenotypeState();
	}

	public static void main(String[] args) throws IOException, OWLOntologyStorageException, HPOException, InterruptedException {
		String config_dir = "/var/lib/rekvisisjon";
		if(args.length > 0)
			config_dir = args[0];
		CommandLine cmd = new CommandLine(config_dir);
		cmd.init();

		BufferedReader reader =
				new BufferedReader(new InputStreamReader(System.in));
		String input;
		System.out.println(readyString);
		while((input = reader.readLine()) != null) {
			System.out.println("Processing command...");
			try {
				System.out.print(cmd.receiveCommand(input));
				System.out.println("Command finished");
			} catch (IllegalArgumentException e) {
				System.err.println("Invalid commandline: " + e.toString());
			}
			System.out.println(readyString);

		}
	}

	/**
	 * Returns HP owl class given phenotype code from HP or UMLS
	 * @param pheno_code
	 * @return
	 * @throws HPOException 
	 * @throws IOException 
	 */
	public OWLClass getHPOEntry(String pheno_code) throws IllegalArgumentException {
		OWLClass ret_class;
		if(pheno_code.substring(0, 4).equals("UMLS")){
			ret_class = ontology.getUMLSEntry(pheno_code);
			if(ret_class == null) {
				throw new IllegalArgumentException("Cannot translate \"" + pheno_code +"\" to HPO");
			}
		} else if(pheno_code.substring(0, 2).equals("HP")) {
			ret_class =  ontology.getOntClass(pheno_code);
		} else {
			throw new IllegalArgumentException("Wrong class \"" + pheno_code + "\". Only HP and UMLS are allowed.");
		}
		return ret_class;
	}

	public PhenotypeTree readMMIFile(String filename) throws OWLOntologyStorageException, IOException, HPOException {
		FileReader fr = new FileReader(filename);
		BufferedReader br = new BufferedReader(fr); 
		String st; 
		PhenotypeTree phenos = PhenotypeTree.createRoot();
		while ((st = br.readLine()) != null) { 
			String cols[] = st.split("\\|");
			if(cols.length < 6)
				System.err.println("Problem parsing line \"" + st + "\"");
			else {
				String pheno_code = cols[4];
				String umls_code = "UMLS:" + pheno_code;
				try {
					OWLClass cls = this.getHPOEntry(umls_code);
					phenos = phenos.insert(cls);
					System.out.println("Translated \"" + umls_code + "\" to \"" + cls.getIRI().getFragment() + "\"");
				} catch(IllegalArgumentException e) {
					//No translation
					//System.out.println("No translation for \"" + umls_code);

				}
			}
		} 
		br.close();
		fr.close();
		return phenos;
	}


	private List<OWLClass> getSubclassOrder(PhenotypeTree phenos, int level) throws OWLOntologyStorageException, HPOException, IOException {
		List<OWLClass> output = new ArrayList<OWLClass>();
		if(level > 0) {	
			Iterator<PhenotypeTree> iter = phenos.getChildren();
			while(iter.hasNext())
				output.addAll(this.getSubclassOrder(iter.next(), level-1));
		} else {
			output.add(phenos.getHpoClass());
		}
		return output;
	}


	public List<OWLClass> getSubclassOrder(PhenotypeTree phenos) throws OWLOntologyStorageException, HPOException, IOException {
		List<OWLClass> output = new ArrayList<>();
		List<OWLClass> next_output;
		int l = 0; 
		do {
			next_output = this.getSubclassOrder(phenos, l);
			output.addAll(next_output);
			l++;
		} while (next_output.size() > 0);
		return output;
	}

	/**
	 * Returns the input list if it is shorter than k, otherwise the sublist with k elements from start
	 * @param <T>
	 * @param l Input list of any length
	 * @param k Max length of returned list
	 */
	public static <T> List<T> list_head(List<T> l, int k){
		return (l.size() < k) ?  l : l.subList(0, k);
	}

	public List<Float> experiment(String umls_disease_code, String mmi_filename, int k) throws IOException, OWLOntologyStorageException, IOException, HPOException {
		PhenotypeTree doc_phenos = this.readMMIFile(mmi_filename);
		List<OWLClass> doc_hpcls = this.getSubclassOrder(doc_phenos);

		UMLS_MRCONSO_FileReader umls = UMLS_MRCONSO_FileReader.getInstance();
		String omim = umls.getOMIMCode(umls_disease_code);
		HPAnnotationsFileReader annots = HPAnnotationsFileReader.getInstance();
		Set<String> symptom_strings =  annots.getDiseaseAnnotations(omim);
		Set<OWLClass> symptom_set = new HashSet<OWLClass>();
		for(String symptom : symptom_strings) {
			symptom_set.add(this.getHPOEntry(symptom));
		}

		String output = "";
		List<Float> precs = new ArrayList<Float>();
		for(int i = 0; i < doc_hpcls.size(); i++) {
			ps.setPositivePatientPhenotype(doc_hpcls.get(i));
			try {
				List<OWLClass> sympt_list = ps.getSymptomSuggestions().stream()
						.map(s -> this.getHPOEntry(s.getHpoLocalName()))
						.collect(Collectors.toList());
				sympt_list = list_head(sympt_list, k);
				float prec = this.precision(symptom_set, sympt_list);
				precs.add(prec);
				output += "After " + doc_hpcls.get(i) + " precision: " + prec; 
			} catch(IOException e) {
				;
			}
		}
		return precs;
	}


	public String getPreOrderSuggestions(PhenotypeTree phenos) throws OWLOntologyStorageException, HPOException, IOException {
		this.setPhenotype(phenos.getHpoClass(), "YES");
		String output = "";
		for(Tickbox sugg : ps.getSymptomSuggestions())
			output += sugg.getHpoLocalName() + "\n";
		Iterator<PhenotypeTree> iter = phenos.getChildren();
		while(iter.hasNext())
			output += getPreOrderSuggestions(iter.next());
		return output;
	}

	public String receiveCommand(String input) throws IOException, IllegalArgumentException, OWLOntologyStorageException, HPOException {
		String[] words = input.split("\\s+");
		if(words.length < 1)
			throw new IllegalArgumentException("Too little input in \"" + input + "\"");
		String output = "";
		String operation = words[0];
		if(operation.equals("setPhenotype")) {
			if(words.length != 3)
				throw new IllegalArgumentException("Wrong number of arguments in input \"" + input + "\"");
			setPhenotype(this.getHPOEntry(words[1]), words[2]);
		} else if(operation.contentEquals("testSuggestions")) {
			try {
				output += this.experiment(words[1], words[2], Integer.getInteger(words[3]));
			} catch(FileNotFoundException e) {
				throw new IllegalArgumentException("Cannot open file \"" + words[1]);
			}
		} else if(operation.contentEquals("readMMIFile")) {
			try {
				output += this.readMMIFile(words[1]);
			} catch(FileNotFoundException e) {
				throw new IllegalArgumentException("Cannot open file \"" + words[1]);
			}
		} else if(operation.contentEquals("setPhenotypeList")) {
			for(int i = 1; i < words.length; i++)
				setPhenotype(this.getHPOEntry(words[i]), "YES");
		} else if(operation.contentEquals("getSuggestions")) {
			for(Tickbox sugg : ps.getSymptomSuggestions())
				output += sugg.getHpoLocalName() + "\n";
		} else if(operation.contentEquals("getDiseases")) {
			for(DiagnosisBean an : ps.getAnnotations())
				output += an.toString() + "\n";
		} else if(operation.contentEquals("listPhenotypes")) {
			PhenotypeOntologySequoia hp = (PhenotypeOntologySequoia) OntologyFactory.getOntology();
			Set<IRI> neg_selections = ps.getPatientNegPhenotypes();
			Set<IRI> selections = ps.getPatientPhenotypes();
			selections.addAll(neg_selections);
			List<Tickbox> sel_list = hp.iriToTickbox(new ArrayList<IRI>(selections), false);
			for(Tickbox t : sel_list) 
				output += t.getHpoLocalName() + "\n";
		} else if(operation.equals("reset")) {
			this.reset();
			output += "Deleted all phenotypes";
		} else {
			throw new IllegalArgumentException("Wrong operation in input \"" + input + "\"");
		}
		return output;
	}


	/**
	 * Average precision at khttps://en.wikipedia.org/wiki/Evaluation_measures_(information_retrieval)#Average_precision
	 * @param omim_code Disease UMLS code
	 * @return
	 */
	public float AveragePrecisionAtK(Set<OWLClass> symptoms, List<OWLClass> suggestions, int k) {
		return AveragePrecision(symptoms, suggestions.subList(0, k)); 
	}

	/**
	 * Average precision https://en.wikipedia.org/wiki/Evaluation_measures_(information_retrieval)#Average_precision
	 * @param omim_code Disease UMLS code
	 * @return
	 */
	public float AveragePrecision(Set<OWLClass> symptoms, List<OWLClass> suggestions) {
		if(symptoms.size() == 0)
			return 0;
		float avep = 0;
		for(int i = 0; i < suggestions.size(); i++) {
			if(symptoms.contains(suggestions.get(i))) {
				avep += precision(symptoms, suggestions.subList(0, i+1));
			}
		}
		return avep / (float) symptoms.size(); 
	}

	/**
	 * Precision of suggested hpo codes compared with actual disease
	 * https://stackoverflow.com/questions/54966320/mapk-computation
	 */
	public float precision(Set<OWLClass> symptom_set, List<OWLClass> doc_hpcls) {
		int correct = 0;
		for(OWLClass t : doc_hpcls) {
			if(symptom_set.contains(t))
				correct++;
		}
		if(doc_hpcls.size() == 0)
			return 0;
		else 
			return ((float) correct) / (float) doc_hpcls.size();
	}

	/**
	 * Precision of suggested hpo codes compared with actual disease
	 * https://stackoverflow.com/questions/54966320/mapk-computation
	 */
	public float recall(Set<OWLClass> symptoms, List<OWLClass> suggestions) {
		if(symptoms.size() == 0)
			return 1;
		int recalled = 0;
		for(OWLClass symptom : symptoms) {
			if(suggestions.contains(symptom))
				recalled++;
		}
		return ((float) recalled) / (float) symptoms.size();
	}

	/**
	 * Gets the HPO codes of all symptoms of a disease
	 * @param omim_code
	 * @return
	 */
	public Set<String> getDiseaseSymptoms(String umls_code){
		UMLS_MRCONSO_FileReader umls = UMLS_MRCONSO_FileReader.getInstance();
		String omim_code = umls.getOMIMCode(umls_code);
		return HPAnnotationsFileReader.getInstance().getDiseaseAnnotations(omim_code);
	}

	public boolean setPhenotype(OWLClass hpo_class, String value) throws OWLOntologyStorageException, HPOException, IOException{
		if(value.equals("YES")) {
			ps.removePatientNegPhenotype(hpo_class);
			//ps.removePatientNegPhenotypeReason(hpo_code);
			return ps.setPositivePatientPhenotype(hpo_class);
		} else if (value.equals("NO") || 
				value.equals(ResponseType.NOT_PRESENT.toString())
				) {
			ps.removePatientPositivePhenotype(hpo_class);
			//ps.removePatientNegPhenotypeReason(hpo_code);
			return ps.setNegativePatientPhenotype(hpo_class);
		} else if (value.equals("NA") || 
				value.equals(ResponseType.CANNOT_CHECK.toString()) ||
				value.equals(ResponseType.CANNOT_PRESENT.toString())  ||
				value.equals(ResponseType.TOO_YOUNG.toString())
				){
			ps.removePatientPositivePhenotype(hpo_class);
			ps.removePatientNegPhenotype(hpo_class);
			//ps.setNegativePhenotypeReason(hpo_code, ResponseType.valueOf(value));
			return true;
		} else
			throw new HPOException("Invalid value of parameter \"value\": " + value + ". valid values are YES, NO and NA.");
	}

	public PhenotypeState getPs() {
		return ps;
	}

	public void setPs(PhenotypeState ps) {
		this.ps = ps;
	}
	
	public void runAllExperiments() throws OWLOntologyStorageException, IOException, HPOException, InterruptedException {
		//http://downloads.hindawi.com/journals/crig/2020/3256539.xml
		String test_resource_dir = "/var/lib/rekvisisjon";

		CommandLine cmd = new CommandLine(test_resource_dir);
		cmd.init();
		
		List<Float> precs1 = cmd.experiment("C1527231", "src/test/resources/3256539.mmi", 10);
		System.out.println(precs1.get(0));
		System.out.println(precs1.get(precs1.size()-1));
		List<Float> precs2 = cmd.experiment("C0265482", "src/test/resources/5957415.mmi", 10);
		System.out.println(precs2.get(0));
		System.out.println(precs2.get(precs2.size()-1));
		List<Float> precs3 = cmd.experiment("C3502510", "src/test/resources/6143050.mmi", 10);
		System.out.println(precs3.get(0));
		System.out.println(precs3.get(precs3.size()-1));
		List<Float> precs4 = cmd.experiment("C4225334", "src/test/resources/8414857.mmi", 10);
		System.out.println(precs4.get(0));
		System.out.println(precs4.get(precs4.size()-1));
	}
}
